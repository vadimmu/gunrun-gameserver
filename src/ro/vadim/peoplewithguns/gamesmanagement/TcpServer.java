package ro.vadim.peoplewithguns.gamesmanagement;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;

import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.MessageHandler;
import ro.vadim.peoplewithguns.communication.messages.Messages;
import ro.vadim.peoplewithguns.communication.receiver.MessageReceiver;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.communication.sender.MessageSenderUtils;
import ro.vadim.peoplewithguns.game.GameManager;
import ro.vadim.peoplewithguns.game.Player;

public class TcpServer implements Runnable{
	
	
	
	
	private final int DEFAULT_PORT = 9001;
	public int PORT = -1;
	
	
	private BufferedReader fromClient = null;
	private BufferedWriter toClient = null;
	private ServerSocket serverSocket = null;
	
	
	
	
	public TcpServer(){		
				
	}
		
	private void init() throws IOException{
		if(PORT != -1)
			setServerSocket(new ServerSocket(PORT));
		else 
			setServerSocket(new ServerSocket(DEFAULT_PORT));
		
		getServerSocket().setSoTimeout(0);
	}
		
	
	
	
	private static void closeSocket(Socket clientSocket){
		try {
			clientSocket.shutdownInput();
			clientSocket.shutdownOutput();
			clientSocket.close();				
		} 
		
		catch (IOException e1) {
			System.out.println("Cannot close connection...");
		}
	}
		
	private static void createMessageReceiver(Socket clientSocket){
		
		System.out.println("TcpServer : Creating MessageReceiver for "+clientSocket.toString());
	    Thread receiverThread = new Thread(new ServerMessageReceiver(clientSocket));
	    receiverThread.start();
	}

	private static void createMessageSender(Socket clientSocket) throws IOException{
		
		System.out.println("TcpServer : Creating MessageSender for "+clientSocket.toString());		
		ServerMessageSender outgoingMessageHandler = new ServerMessageSender(clientSocket);		
	}
	
	private static void addClient(Socket clientSocket) throws IOException{
				
        createMessageReceiver(clientSocket);
        createMessageSender(clientSocket);
	}
	
	
	
	
	private void start(){
		
		try {
			init();
		
			while(!getServerSocket().isClosed())
	        {							
				Socket clientSocket = getServerSocket().accept();
				System.out.println("Accepted client. Address: "+clientSocket.getInetAddress().toString()+":"+String.valueOf(clientSocket.getPort()));			
				clientSocket.setKeepAlive(true);
				
				addClient(clientSocket);				
	        }
		}
		catch (IOException e) {
			System.out.println("TcpServer : could not start : "+e.toString());
		}
	}

	@Override
	public void run() {	
		start();		
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}
	
	

}
