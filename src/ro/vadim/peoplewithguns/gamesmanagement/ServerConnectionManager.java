package ro.vadim.peoplewithguns.gamesmanagement;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.receiver.MessageReceiver;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;

public class ServerConnectionManager {
	
	public static final int EXPECTED_CONNECTIONS = 50;	
	
	public static HashMap<InetSocketAddress, UUID> connections = new HashMap<InetSocketAddress, UUID>(EXPECTED_CONNECTIONS);
	
	
	public static HashMap<InetSocketAddress, ServerMessageReceiver> messageReceiverList = new HashMap<InetSocketAddress, ServerMessageReceiver>(EXPECTED_CONNECTIONS);
	public static HashMap<InetSocketAddress, ServerMessageSender> messageSenderList = new HashMap<InetSocketAddress, ServerMessageSender>(EXPECTED_CONNECTIONS);
	
	private GameServer gameServer = null;
	
	
	
	
	
	public static void replacePreviousConnection(InetSocketAddress newPlayerAddress, UUID newPlayerUUID){
		
		for(UUID id : ServerConnectionManager.getConnections().values()){
			if(newPlayerUUID.equals(id)){
				System.out.println("This ID already exists ! Closing previous connection...");
				
				System.out.println("Registering connection...");
				InetSocketAddress oldPlayerAddress = ServerConnectionManager.getAddressByUUID(id);
				ServerConnectionManager.closeConnection(oldPlayerAddress);				
				ServerConnectionManager.getConnections().put(newPlayerAddress, newPlayerUUID);
				return;
			}
		}
		
		System.out.println("Registering connection...");
		ServerConnectionManager.getConnections().put(newPlayerAddress, newPlayerUUID);
	}
	
	
	public static synchronized ServerMessageSender getMessageSender(InetSocketAddress address){
		if(messageSenderList.containsKey(address))
			return messageSenderList.get(address);
		
		return null;
	}	
	public static synchronized void removeMessageSender(InetSocketAddress address) throws IOException{
		if(messageSenderList.containsKey(address)){			
			System.out.println("closeConnection : closing MessageSender");
			messageSenderList.get(address).stop();
			System.out.println("closeConnection : removing MessageSender");
			messageSenderList.remove(address);
		}
	}	
	public static synchronized void addMessageSender(ServerMessageSender sender, InetSocketAddress address){
		messageSenderList.put(address, sender);		
	}
	
	
	
	public static synchronized ServerMessageReceiver getMessageReceiver(InetSocketAddress address){
		if(messageReceiverList.containsKey(address))
			return messageReceiverList.get(address);
		
		return null;
	}
	public static synchronized void removeMessageReceiver(InetSocketAddress address){
		if(messageReceiverList.containsKey(address)){			
			System.out.println("closeConnection : closing MessageReceiver");
			
			ServerMessageReceiver ms = messageReceiverList.get(address);
			
			if(ms.stop == false){
				ms.stop();				
			}
			
			System.out.println("closeConnection : removing MessageReceiver");
			messageReceiverList.remove(address);
		}
		
	}
	public static synchronized void addMessageReceiver(ServerMessageReceiver receiver, InetSocketAddress address){
		messageReceiverList.put(address, receiver);
	}
	
	
	
	public static synchronized void removeConnection(InetSocketAddress address){
		if(connections.containsKey(address)){
			System.out.println("closeConnection : removing connection record");
			connections.remove(address);
		}
	}	
	public static synchronized void addConnection(Socket clientSocket, UUID clientUUID){		
		InetSocketAddress newClientAddress = new InetSocketAddress(clientSocket.getInetAddress(), clientSocket.getPort());		
		connections.put(newClientAddress, clientUUID);
	}	
	public static synchronized UUID getUuidByAddress(InetSocketAddress address){
		if(connections.containsKey(address))
			return connections.get(address);
		
		return null;
	}
	
	public static synchronized InetSocketAddress getAddressByUUID(UUID playerID){
		
		for(Entry<InetSocketAddress, UUID> e : connections.entrySet()){
			if(e.getValue().equals(playerID)){
				return e.getKey();			
			}
		}
		
		return null;
	}
		
	public static synchronized void closeConnection(InetSocketAddress clientAddress){
		
		System.out.println("closeConnection : attempting to close the connection");
		
		try{			
						
			UUID playerUUID =  getUuidByAddress(clientAddress);						
			removeMessageSender(clientAddress);
			removeMessageReceiver(clientAddress);
			removeConnection(clientAddress);
			
		}
		catch(IOException e){
			System.out.println("COULD NOT CLOSE THE CONNECTION");
			System.out.println(e.toString());			
		}
	}
	
	public static synchronized HashMap<InetSocketAddress, UUID> getConnections(){
		return connections;
	}
	
	public static synchronized void removeAllConnections(){
		connections.clear();		
	}
	
	public static synchronized void closeAllConnections(){
					
			try{
				for(InetSocketAddress a : getConnections().keySet()){
				
					System.out.println("closeConnection : deleting player");
					
					UUID playerUUID =  getUuidByAddress(a);
					removeMessageSender(a);
					removeMessageReceiver(a);
					
				}				
				connections.clear();				
			}
			
			catch(IOException e){
				System.out.println("COULD NOT CLOSE THE CONNECTION");
				System.out.println(e.toString());			
			}
	}


}

