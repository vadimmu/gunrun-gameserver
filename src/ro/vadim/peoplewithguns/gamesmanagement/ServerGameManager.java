package ro.vadim.peoplewithguns.gamesmanagement;



import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.GameServer;

public class ServerGameManager {
	
	public static int DEFAULT_NUMBER_OF_GAME_SERVERS = 10;
	public static HashMap<UUID, GameServer> gameServers = new HashMap<UUID, GameServer>(DEFAULT_NUMBER_OF_GAME_SERVERS);
	
	
	
	
	
	public synchronized static void registerGameServer(GameServer gameServer){
		
		gameServers.put(gameServer.getServerID(), gameServer);			
		System.out.println("ADDED GAME SERVER TO THE LIST !");
		System.out.println("Number of Game Servers: "+getGameServers().size());
		InetSocketAddress clientAddress = ServerConnectionManager.getAddressByUUID(gameServer.getCreatorID());
		ServerConnectionManager.getMessageSender(clientAddress).send_GAME_CREATED(gameServer);		
		showGamesList();
	}
	
	public synchronized static void showGamesList(){
		
		System.out.println("============== GAME SERVER LIST ============");
		System.out.println("Number of servers: "+gameServers.size());
		for(Entry<UUID, GameServer> e : gameServers.entrySet()){
			
			UUID gameID = e.getKey();
			GameServer gs = e.getValue();
			
			String serverData = "name: "+gs.getName()+" id: "+gameID.toString();
			System.out.println(serverData);
		}
		
		System.out.println("============================================");
	}
		
	public static UUID createGame(String name, UUID creatorUUID, Integer gameType){
		
		String serverName = "";
		
		if(name == null)
			serverName = "Game"+String.valueOf(gameServers.size()+1);
		
		else if(name.trim().equals("")){
			serverName = "Game"+String.valueOf(gameServers.size()+1);
		}
		
		else
			serverName = name;
		
		UUID gameID = UUID.randomUUID();
		
		if(gameType == null)
			gameType = GameServer.GAME_TYPE_NORMAL;		
		
		
		GameServer newGameServer = new GameServer(creatorUUID, gameID, serverName, gameType);
		
		Thread newGameThread = new Thread(newGameServer);
		newGameThread.start();		
		
		return gameID;
	}
	
	
	public synchronized static void removeGame(UUID gameServerID){
		
		if(!gameServers.containsKey(gameServerID)){
			System.out.println("Game ID not present in the game records...");
			return;
		}
		
		gameServers.remove(gameServerID);
	}
	
	
	
	
	
	
	
	
	
	public static HashMap<UUID, GameServer> getGameServers() {
		return new HashMap<UUID, GameServer>(gameServers);
	}	
	
	public static void setGameServers(HashMap<UUID, GameServer> gameServers) {
		ServerGameManager.gameServers = gameServers;
	}
	
}
