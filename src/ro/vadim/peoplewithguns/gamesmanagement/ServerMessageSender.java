package ro.vadim.peoplewithguns.gamesmanagement;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.Messages;


import com.fasterxml.jackson.core.JsonProcessingException;

public class ServerMessageSender{
	
	private OutputStream toClient_Stream = null;
	private BufferedWriter toClient_Writer = null;	
	private InetSocketAddress client_Address = null;
	private UUID clientUUID = null;	
	
	
	
	public void stop(){
		
		try{
			toClient_Writer.flush();
			toClient_Writer.close();
		}
		
		catch(IOException e){			
			System.out.println("Cannot stop MessageSender");			
		}
	}
		
	public ServerMessageSender(Socket socket){
				
		
		try{
			toClient_Stream = socket.getOutputStream();
			toClient_Writer = new BufferedWriter(new OutputStreamWriter(toClient_Stream));
			client_Address = new InetSocketAddress(socket.getInetAddress(), socket.getPort());
			
			ServerConnectionManager.addMessageSender(this, client_Address);			
			
		}
		
		catch(IOException e){
			System.out.println("MessageSender(constructor) Error : "+e.toString());			
		}
		
	}	
	
	
	private void sendMessage(String message){
		if(message!=null)
			try {
				toClient_Writer.write(message+'\n');
				toClient_Writer.flush();
				
			}		
			catch (IOException e) {
				System.out.println("sendMessage ERROR ! : "+e.toString());
				System.out.println("sendMessage ERROR ! : original message : "+message);
				System.out.println("MessageSender : sendMessage : removing player");
				
				ServerConnectionManager.closeConnection(client_Address);
			}
	}
	
	private void sendMessage(Map<String, Object> jsonStructure){
		try {
			sendMessage(ServerMessageSenderUtils.mapper.writeValueAsString(jsonStructure)+'\n');
		} 
		
		catch (JsonProcessingException e) {
			
			System.out.println("sendmMessage : Json Error : "+e.toString());
			
		}
	}
	
	
	
//	public static int GAMES_LIST = 26;				//[{game: GameServerID, name: GameServerName, ip: GameServerIp, port: GameServerPort, inProgress: booleanGameIsInProgress, countAwayTeam: noOfPlayersInAwayTeam, countHomeTeam, noOfPlayersInHomeTeam},{...}]
//	public static int GAME_CREATED = 27;			//{game: GameServerID, name: GameServerName, ip: GameServerIP, port: GameServerPort}
//	public static int HELLO = 28;					//{player: playerUUID}
	
	public void send_GAMES_LIST(){
		System.out.println("Sending GAMES LIST");		
		
		try {
			sendMessage(ServerMessageSenderUtils.composeGameServerListMessage());
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_GAMES_LIST : Json Error : "+e.toString());
		}
	}
	
	public void send_GAME_CREATED(GameServer gameServer){
		
		System.out.println("Sending GAME CREATED");		
		
		try {
			sendMessage(ServerMessageSenderUtils.composeGameCreatedMessage(gameServer));
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_GAMES_CREATED : Json Error : "+e.toString());
		}
	}
	
	public void send_HELLO(UUID clientUUID){
		System.out.println("Sending HELLO with the player's ID");	
		
		try {
			sendMessage(ServerMessageSenderUtils.composeHelloMessage(clientUUID));
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_HELLO : Json Error : "+e.toString());
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	private void print(String message){
		System.out.println(message);		
	}
}

