package ro.vadim.peoplewithguns.gamesmanagement;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;


import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.Messages;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ServerMessageSenderUtils {
	
	public static ObjectMapper mapper = null;
	
	
	
	
	
	public static void initObjectMapper(){
		mapper = new ObjectMapper();		
	}
	
	
	
	
	
	public static String composeEmptyMessage(int messageType){
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : {} }"+'\n';
	}
	
	public static String composeMessage(int messageType, Map<String, Object> innerStructure) throws JsonProcessingException{
		
		String data = mapper.writeValueAsString(innerStructure);
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : "+ data + " }"+'\n';
	}
	
	public static String composeMessage(int messageType, String data) throws JsonProcessingException{
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : "+ data + " }"+'\n';
	}
		
	public static String jsonMapToString(Map<String, Object> jsonStructure) throws JsonProcessingException{		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	public static String jsonListToString(ArrayList<Object> jsonStructure) throws JsonProcessingException{		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	
	
	
	
	public static String composeGameServerListMessage() throws JsonProcessingException{
				
		
		ArrayList<Object> messageJsonStructure = new ArrayList<Object>(ServerGameManager.getGameServers().size());
		
		System.out.println("================= LIST OF GAME SERVERS =================");
		
		for(Entry<UUID, GameServer> e : ServerGameManager.getGameServers().entrySet()){
			
			GameServer gs = e.getValue();
			UUID gsID = e.getKey();			
			
			LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
			jsonStructure.put("game", gs.getServerID());
			jsonStructure.put("creatorID", gs.getCreatorID());
			jsonStructure.put("name", gs.getName());
			jsonStructure.put("ip", gs.getIP());
			jsonStructure.put("port", gs.getPort());
			jsonStructure.put("inProgress", String.valueOf(gs.isGameInProgress()));
			jsonStructure.put("countAwayTeam", String.valueOf(gs.getAwayTeamCount()));
			jsonStructure.put("countHomeTeam", String.valueOf(gs.getHomeTeamCount()));
			jsonStructure.put("gameType", String.valueOf(gs.getGameType()));
			
			
			String serverData = jsonMapToString(jsonStructure);
			
			messageJsonStructure.add(serverData);
			
			System.out.println(gs.getName());
			
		}
				
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("gamesList", jsonListToString(messageJsonStructure));
		return composeMessage(Messages.Admin.FromServer.GAMES_LIST, jsonMapToString(jsonStructure));
	}
	
	
	public static String composeGameCreatedMessage(GameServer gameServer) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("game", gameServer.getServerID());
		jsonStructure.put("name", gameServer.getName());
		jsonStructure.put("ip", gameServer.getIP());
		jsonStructure.put("port", gameServer.getPort());
		jsonStructure.put("gameType", gameServer.getGameType());
		
		return composeMessage(Messages.Admin.FromServer.GAME_CREATED, jsonMapToString(jsonStructure));		
	}
	
	
	public static String composeHelloMessage(UUID clientUUID) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		jsonStructure.put("player", clientUUID.toString());
		
		return composeMessage(Messages.Admin.FromServer.HELLO, jsonMapToString(jsonStructure));
		
	}
	
	
	
}
