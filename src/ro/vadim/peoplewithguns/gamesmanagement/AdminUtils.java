package ro.vadim.peoplewithguns.gamesmanagement;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;

public class AdminUtils {

	public static final int MIN_PORT_NUMBER = 9002;
	public static final int MAX_PORT_NUMBER = 30002;
		
	
	public static boolean available(int port) {
	    if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
	        throw new IllegalArgumentException("Invalid start port: " + port);
	    }

	    ServerSocket ss = null;
	    DatagramSocket ds = null;
	    try {
	        ss = new ServerSocket(port);
	        ss.setReuseAddress(true);
	        ds = new DatagramSocket(port);
	        ds.setReuseAddress(true);
	        return true;
	    }
	    
	    catch (IOException e) {
	    
	    } 
	    
	    finally {
	        if (ds != null) {
	            ds.close();
	        }

	        if (ss != null) {
	            try {
	                ss.close();
	            } catch (IOException e) {
	                /* should not be thrown */
	            }
	        }
	    }

	    return false;
	}
	
}
