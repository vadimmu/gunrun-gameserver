package ro.vadim.peoplewithguns.gamesmanagement;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.MessageHandler;
import ro.vadim.peoplewithguns.communication.messages.Messages;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.GameManager;
import ro.vadim.peoplewithguns.game.Player;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ServerMessageReceiver implements Runnable{

	
	private Socket socket = null;
	
	private InputStream fromServer_Stream = null;
	private BufferedReader fromServer_Reader = null;	
	private ServerMessageSender messageSender = null;
	private InetSocketAddress clientAddress = null; 
	private UUID clientUUID = null;
	
		
	public boolean stop = false;
	
	public ServerMessageReceiver(Socket socket) {
		this.socket = socket;		
		clientAddress = new InetSocketAddress(socket.getInetAddress(), socket.getPort());
		if(ServerMessageSenderUtils.mapper == null)
			ServerMessageSenderUtils.mapper = new ObjectMapper();
		
		
		ServerConnectionManager.addMessageReceiver(this, clientAddress);				
	}
	
	private ServerMessageSender getMessageSender(){
		InetSocketAddress remoteAddress = new InetSocketAddress(socket.getInetAddress(), socket.getPort());		
		return ServerConnectionManager.messageSenderList.get(remoteAddress);
	}
	
	public Map<String, Object> decodeMessage(String message) throws JsonParseException, JsonMappingException, IOException{		
		
		Map<String,Object> jsonObject = ServerMessageSenderUtils.mapper.readValue(message, Map.class);
		
		return jsonObject;
	}
	
	public void on(String messageType, Map<String, Object> jsonObject, MessageHandler messageManager){
		
		if(messageSender == null)
			messageSender = getMessageSender();
		
		if(!jsonObject.containsKey("messageType")){
			System.out.println("the Json object passed as an argument does not have a \"messageType\" field.");			
			return;	
		}
			
		
		if(jsonObject.get("messageType").equals(messageType)){			
			
			if(jsonObject.containsKey("data")){				
				messageManager.manageMessage((Map<String, Object>) jsonObject.get("data"));
			}
			else{
				System.out.println("correct message type, but no data...");
			}
		}
	}
	
	public void print(String message){
		System.out.println(message);
	}
	
	public void handleMessages(Socket socket){
		
		try {
			
			fromServer_Stream = socket.getInputStream();
			fromServer_Reader = new BufferedReader(new InputStreamReader(fromServer_Stream));
			
			String receivedMessage = "";
			
			
			final Socket remoteSocket = socket;
			
			while(((receivedMessage = fromServer_Reader.readLine()) != null) && (stop == false)){
				
				print("RECEIVED MESSAGE : "+receivedMessage);
				
				Map<String, Object> messageObject = decodeMessage(receivedMessage);
				
				
				on(String.valueOf(Messages.Admin.ToServer.HELLO), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						print("received HELLO");
						
						if(ServerMessageSenderUtils.mapper == null){
							System.out.println("ObjectMapper is null. Initializing...");
							ServerMessageSenderUtils.initObjectMapper();
						}
						
						if(!jsonData.containsKey("player"))
							return;
							
							
						String playerUUIDString = String.valueOf(jsonData.get("player"));
						
						UUID playerUUID = null;
						
						if(playerUUIDString.trim().equals("null"))
							playerUUID = UUID.randomUUID();
						else
							try{
								playerUUID = UUID.fromString(playerUUIDString);
							}
							catch(IllegalArgumentException e){
								playerUUID = UUID.randomUUID();
							}
						
						clientUUID = playerUUID;
						
						ServerConnectionManager.replacePreviousConnection(clientAddress, playerUUID);						
						
						ServerConnectionManager.getMessageReceiver(clientAddress).clientUUID = playerUUID;
						ServerConnectionManager.getMessageSender(clientAddress).send_HELLO(playerUUID);
						
						
						
					}
				});
			
			
				on(String.valueOf(Messages.Admin.ToServer.CREATE_GAME), messageObject, new MessageHandler(){

					@Override
					public void manageMessage(Map<String, Object> jsonData) {						
						System.out.println("received CREATE_GAME");
						
						if(!ServerConnectionManager.getConnections().containsKey(clientAddress)){
							System.out.println("this connection is not registered !");
							return;
						}
						
						UUID creatorUUID = ServerConnectionManager.getConnections().get(clientAddress);
						Integer gameType = null; 
						
						
						
						if(!jsonData.containsKey("name"))
							return;
						
						if (!jsonData.containsKey("gameType"))
							gameType = GameServer.GAME_TYPE_NORMAL; 
						else
							gameType = Integer.valueOf(String.valueOf(jsonData.get("gameType")));
													
						
						if((gameType.intValue() != GameServer.GAME_TYPE_NORMAL) &&
								(gameType.intValue() != GameServer.GAME_TYPE_DUEL) &&
								(gameType.intValue() != GameServer.GAME_TYPE_DAVID_VS_GOLIATH))
							
							gameType = GameServer.GAME_TYPE_NORMAL;
						
						UUID gameID = ServerGameManager.createGame(String.valueOf(jsonData.get("name")), creatorUUID,  gameType);
						
						GameServer newGame = ServerGameManager.getGameServers().get(gameID);
						
					}
				});
			
				
				on(String.valueOf(Messages.Admin.ToServer.GAMES_LIST), messageObject, new MessageHandler(){

					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						
						ServerConnectionManager.getMessageSender(clientAddress).send_GAMES_LIST();
					}
					
				});
				
			}
			
			
		} 
		
		catch (JsonParseException e) {
			System.out.println("MessageReceiver : "+e.toString());
		} 
		
		catch (JsonMappingException e) {
			System.out.println("MessageReceiver : "+e.toString());
		} 
		
		catch (IOException e) {			
			System.out.println("MessageReceiver : "+e.toString());			
		}		
		
		
	}
	
	
	@Override
	public void run() {
		handleMessages(socket);			
	}
	
	public void stop(){
		
		try{
			stop = true;			
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
			
		}
		catch(Exception e){
			System.out.println("MessageReceiver(Stop) : could not close connection properly.");
			System.out.println("MessageReceiver(Stop) : "+e.toString());
			System.out.println("MessageReceiver(Stop) : removing player");
			ServerConnectionManager.closeConnection(clientAddress); ///////////// PROBLEME AICI !
		}
	}


}
