package ro.vadim.peoplewithguns.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;

import ro.vadim.peoplewithguns.communication.messages.Messages;
import ro.vadim.peoplewithguns.communication.receiver.MessageReceiver;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.communication.sender.MessageSenderUtils;
import ro.vadim.peoplewithguns.game.GameManager;
import ro.vadim.peoplewithguns.game.Player;
import ro.vadim.peoplewithguns.gamesmanagement.AdminUtils;
import ro.vadim.peoplewithguns.gamesmanagement.ServerConnectionManager;
import ro.vadim.peoplewithguns.gamesmanagement.ServerGameManager;
import ro.vadim.peoplewithguns.gamesmanagement.TcpServer;

public class GameServer implements Runnable{
	
	public final static int GAME_TYPE_NORMAL = 0; 
	public final static int GAME_TYPE_DUEL = 1;
	public final static int GAME_TYPE_DAVID_VS_GOLIATH = 2;
	
	private int DEFAULT_PORT = 9002;
	private int PORT = AdminUtils.MIN_PORT_NUMBER;
	
	private String serverName = null;	
	private UUID serverID = null;
	private UUID creatorID = null;
	private Integer gameType = null; 
	
	private BufferedReader fromClient = null;
	private BufferedWriter toClient = null;
	private ServerSocket serverSocket = null;
	
	
	private Thread incomingMessageThread = null;
	
		
	private MessageReceiver messageReceiver = null;		
	private ConnectionManager connectionManager = null;
	private GameManager gameManager = null;
	
	
	private MessageSenderUtils messageSenderUtils = null;
	private GameServerUtils gameServerUtils = null;
	
	
	
	public GameServer(UUID newCreatorID, UUID newServerID, String newServerName, int newGameType){
				
		while(!AdminUtils.available(PORT)){
			PORT ++;
		}
				
		if(newServerName == null)
			setName("Game "+String.valueOf(ServerGameManager.getGameServers().size()+1));		
		else
			setName(newServerName);
		
		setCreatorID(newCreatorID);
		
		setServerID(newServerID);
		
		setGameType(newGameType);
		
		
		createMessageSenderUtils();
		createConnectionManager();
		createGameManager();
		createGameServerUtils();
		
		init();
	}
		
	private void init(){
				
		try {
			serverSocket = new ServerSocket(PORT);
			serverSocket.setSoTimeout(0);
			ServerGameManager.registerGameServer(this);
		} 
		
		catch (IOException e) {
			System.out.println("GameServer: Could not initialize ! Error: "+e.toString());
		}
		
	}
		
	private void generatePlayer(Socket clientSocket, UUID newPlayerUUID){
		Player newPlayer = new Player("Player", 100, "Marine", null, null); 
		getConnectionManager().addConnection(clientSocket, newPlayerUUID);		
		getGameManager().addPlayer(newPlayer, newPlayerUUID);		
		
	}	
		
	private void createMessageReceiver(Socket clientSocket, UUID newPlayerUUID){
		
		setMessageReceiver(new MessageReceiver(this, clientSocket, newPlayerUUID));
        incomingMessageThread = new Thread(getMessageReceiver());
        incomingMessageThread.start();	
	}
	
	private void createMessageSender(Socket clientSocket) throws IOException{
		MessageSender outgoingMessageHandler = new MessageSender(clientSocket, this);		
	}
	
		
	private void createConnectionManager(){
		setConnectionManager(new ConnectionManager(this));		
	}
	
	private void createGameManager(){		
		setGameManager(new GameManager(this));		
	}
	
	private void createMessageSenderUtils(){
		
		setMessageSenderUtils(new MessageSenderUtils(this));
		getMessageSenderUtils().mapper = new ObjectMapper();
		
	}
	
	private void createGameServerUtils(){
		
		setGameServerUtils(new GameServerUtils(this));		
		
	}
	
	
	
	
	private void addClient(Socket clientSocket, UUID newPlayerUUID) throws IOException{
		generatePlayer(clientSocket, newPlayerUUID);
        createMessageReceiver(clientSocket, newPlayerUUID);
        createMessageSender(clientSocket);        
        MessageSender.send_PLAYER_JOINED(this, newPlayerUUID);
	}
	
	private void welcomeClient(Socket clientSocket, UUID newPlayerUUID) throws IOException{
		
		InetSocketAddress newPlayerAddress = new InetSocketAddress(clientSocket.getInetAddress(), clientSocket.getPort());
		
		for(UUID id : getConnectionManager().getConnections().values()){
			if(newPlayerUUID.equals(id)){
				
				System.out.println("This ID already exists ! Closing previous connection...");
				
				InetSocketAddress oldPlayerAddress = getConnectionManager().getAddressByUUID(id);
				getConnectionManager().closeConnection(oldPlayerAddress);
				addClient(clientSocket, newPlayerUUID);
				return;
			}
		}
		
		addClient(clientSocket, newPlayerUUID);
		
	}
	
	
	public void stopGameServer(){
		
		getConnectionManager().closeAllConnections();
		
		try {
			serverSocket.close();
		}
		
		catch (IOException e) {
			System.out.println("GameServer: Could not close server socket ! Reason: "+e.toString());
		}
	}
	
	private void closeSocket(Socket clientSocket){
		try {
			clientSocket.shutdownInput();
			clientSocket.shutdownOutput();
			clientSocket.close();				
		} 
		
		catch (IOException e1) {
			System.out.println("Cannot close connection...");
		}
	}
	
	private void getHelloMessage(Socket clientSocket){
		InputStream fromServer_Stream = null;
		BufferedReader fromServer_Reader = null;
		
		
		System.out.println("Waiting for the HELLO message ... ");
		
		try {
			fromServer_Stream = clientSocket.getInputStream();
			fromServer_Reader= new BufferedReader(new InputStreamReader(fromServer_Stream));
			
			
			
			System.out.println("Getting HELLO message...");
			String receivedMessage = fromServer_Reader.readLine();
			System.out.println("Got it ! : " + receivedMessage);
			
			if(getMessageSenderUtils().mapper == null){
				System.out.println("ObjectMapper is null. Initializing...");
				getMessageSenderUtils().initObjectMapper();
			}
			
			Map<String,Object> jsonObject = getMessageSenderUtils().mapper.readValue(receivedMessage, Map.class);
			
			if(!jsonObject.containsKey("messageType")){
				System.out.println("the Json object passed as an argument does not have a \"messageType\" field.");			
				return;	
			}
			
			Integer messageType = Integer.valueOf(String.valueOf(jsonObject.get("messageType"))); 			
			
			if(messageType.intValue() == Messages.Admin.ToServer.HELLO){	
				
				System.out.println("ObjectMapper is null. Initializing...");
				
				if(jsonObject.containsKey("data")){
					
					Map<String, Object> jsonData = (Map<String, Object>) jsonObject.get("data");
					
					if(jsonData.containsKey("player")){
						String playerUUIDString = String.valueOf(jsonData.get("player"));
						
						UUID playerUUID = null;
						
						if(playerUUIDString.trim().equals("null"))
							playerUUID = UUID.randomUUID();
						else
							try{
								playerUUID = UUID.fromString(playerUUIDString);
							}
							catch(IllegalArgumentException e){
								playerUUID = UUID.randomUUID();
							}
						
						welcomeClient(clientSocket, playerUUID);
						
					}
					else{
						System.out.println("doesn't contain player");
						closeSocket(clientSocket);
					}
				}
				
				else{
					closeSocket(clientSocket);
					System.out.println("doesn't contain data");
				}
			}
			else{
				closeSocket(clientSocket);
				System.out.println("doesn't have messageType");
			}
			
		} 
		
		catch (IOException e) {
			System.out.println("Cannot establish connection. Closing it...");
			closeSocket(clientSocket);
		} 
		 
			
		
		
		
		
		
	}
	
		
	
	
	
	@Override
	public void run() {	
		try {
			
			System.out.println("GameServer \""+getName()+"\" has started ! ( ID: "+getServerID()+" )");
			
			while(!serverSocket.isClosed())
	        {							
				Socket clientSocket = serverSocket.accept();
								
				clientSocket.setKeepAlive(true);
				
				if(getGameManager().game_entered == false){
				
					System.out.println("Player connected : "+clientSocket.getInetAddress().toString());					
					getHelloMessage(clientSocket);
					
				}
				else{
					System.out.println("There's a game in progress ! Cannot accept player !");
					clientSocket.close();
				}
	        }
		}
		catch (IOException e) {
			System.out.println("TcpServer : could not start : "+e.toString());
		}
	}
	
	
	
	public ConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(ConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public GameManager getGameManager() {
		return gameManager;
	}

	public void setGameManager(GameManager gameManager) {
		this.gameManager = gameManager;
	}

	public MessageSenderUtils getMessageSenderUtils() {
		return messageSenderUtils;
	}

	public void setMessageSenderUtils(MessageSenderUtils messageSenderUtils) {
		this.messageSenderUtils = messageSenderUtils;
	}

	public MessageReceiver getMessageReceiver() {
		return messageReceiver;
	}

	public void setMessageReceiver(MessageReceiver messageReceiver) {
		this.messageReceiver = messageReceiver;
	}

	public UUID getCreatorID() {
		return creatorID;
	}

	public void setCreatorID(UUID creatorID) {
		this.creatorID = creatorID;
	}

	public UUID getServerID() {
		return serverID;
	}

	public void setServerID(UUID serverID) {
		this.serverID = serverID;
	}

	
	
	public int getHomeTeamCount(){		
		return getGameManager().home_team.size();		
	}
	
	public int getAwayTeamCount(){
		return getGameManager().away_team.size();
	}
	
	public boolean isGameInProgress(){
		return getGameManager().game_entered;		
	}
	
	public String getIP(){
		try {
			return InetAddress.getLocalHost().getHostAddress();
		}
		
		catch (UnknownHostException e) {
			
			System.out.println("Could not retrieve my OWN IP address ! Hawkward...");
			return "0.0.0.0";
		}
	}
	
	public String getPort(){
		return String.valueOf(serverSocket.getLocalPort());
	}

	
	public String getName() {
		return serverName;
	}

	
	public void setName(String serverName) {
		this.serverName = serverName;
	}

	public GameServerUtils getGameServerUtils() {
		return gameServerUtils;
	}

	public void setGameServerUtils(GameServerUtils gameServerUtils) {
		this.gameServerUtils = gameServerUtils;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

}
