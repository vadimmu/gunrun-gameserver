package ro.vadim.peoplewithguns.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.peoplewithguns.game.gameelements.GameManager;
import ro.vadim.peoplewithguns.game.gameelements.Player;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



/////////////////////////////////////////////////////// SERVER 
public class MessageSender{
		
	private OutputStream toClient_Stream = null;
	private BufferedWriter toClient_Writer = null;	
	private InetAddress client_Address = null;
	private UUID client_UUID = null;
	
		
	public void stop(){
		
		try{
			toClient_Writer.flush();
			toClient_Writer.close();
		}
		
		catch(IOException e){			
			System.out.println("Cannot stop MessageSender for UUID: "+client_UUID.toString());			
		}
	}
		
	public MessageSender(Socket socket){
		
		try{
			toClient_Stream = socket.getOutputStream();
			toClient_Writer = new BufferedWriter(new OutputStreamWriter(toClient_Stream));
			client_Address = socket.getInetAddress();
			client_UUID = ConnectionManager.getUuidByAddress(client_Address);
		
			ConnectionManager.addMessageSender(this, client_Address);
		
			send_CONFIGURATION();
		}
		catch(IOException e){
			System.out.println("MessageSender(constructor) Error : "+e.toString());			
		}
		
	}	
	
	public static void multicastMessage(String message, int team, UUID senderUUID) throws IOException{
		
		
		
		for(MessageSender sender : ConnectionManager.messageSenderList.values()){
			
			if(senderUUID != null)
				if(!sender.client_UUID.equals(senderUUID))
					if(GameManager.getTeam(sender.client_UUID) != team)
						sender.sendMessage(message);
		}
		
	}
	
	public static void broadcastMessage(String message, UUID senderUUID) throws IOException{
		
		for(MessageSender sender : ConnectionManager.messageSenderList.values()){
			
			if(senderUUID != null){
				if(!sender.client_UUID.equals(senderUUID))
					sender.sendMessage(message);
			}
			else{
				sender.sendMessage(message);
			}
		}
	}
	
	private void sendMessage(String message){
		if(message!=null)
			try {
				toClient_Writer.write(message+'\n');
				toClient_Writer.flush();
				
			}		
			catch (IOException e) {
				System.out.println("sendMessage ERROR ! : "+e.toString());
				System.out.println("sendMessage ERROR ! : original message : "+message);
			}
	}
	
	private void sendMessage(Map<String, Object> jsonStructure){
		try {
			sendMessage(MessageSenderUtils.mapper.writeValueAsString(jsonStructure)+'\n');
		} 
		
		catch (JsonProcessingException e) {
			
			System.out.println("sendmMessage : Json Error : "+e.toString());
			
		}
	}
	
	private void print(String message){
		System.out.println(message);		
	}
	
	
		
	
	//IN-GAME MESSAGES
	/////////////////////////////////////////////////////////
	
	public static int CHANGE_POSITION = 3;		// {player: playerUUID, latitude: newLatitude, longitude: newLongitude}
	public static int SHOOT = 4;				// {player: playerUUID, target: targetUUID, weapon: weapon_name, damage: damageAmount, (optional)timestamp: timeStamp}
	public static int MESSAGE_TEAM = 5;			// {player: playerUUID, message: newMessage}
	public static int GAME_START = 6;			// broadcast message that is sent from the server to let all the players know 
												// that they are sufficiently far from each other and their weapons are now considered active	
	
	public void send_CHANGE_POSITION(double latitude, double longitude){
		print("Sending CHANGE_POSITION");
				
		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(Messages.InGame.FromServer.CHANGE_POSITION, 
						MessageSenderUtils.constructChangePositionString(client_UUID, latitude, longitude)
				),
				client_UUID
			);
		}
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_POSITION : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_CHANGE_POSITION : Network Error : "+e.toString());
		}
	}
	
	public void send_MESSAGE_TEAM(String message){
		print("Sending MESSAGE_TEAM");
		
		int playersTeam = GameManager.getTeam(client_UUID);
		try{
			if(playersTeam == GameManager.HOME_TEAM){
				multicastMessage(MessageSenderUtils.composeMessage(Messages.InGame.FromServer.MESSAGE_TEAM, message), GameManager.HOME_TEAM, client_UUID);
			}
			else if(playersTeam == GameManager.AWAY_TEAM){
				multicastMessage(MessageSenderUtils.composeMessage(Messages.InGame.FromServer.MESSAGE_TEAM, message), GameManager.AWAY_TEAM, client_UUID);
			}
		}
		
		catch (JsonProcessingException e) {
			System.out.println("send_MESSAGE_TEAM : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_MESSAGE_TEAM : Network Error : "+e.toString());
		}
	}
	
	public static void send_GAME_START(){
		System.out.println("Sending GAME_START");
		// IF THE MINIMUM DISTANCE BETWEEN PLAYERS OF BOTH TEAMS IS AT LEAST X (EG. 1000M), THE GAME SHOULD START		
		
		try {
			broadcastMessage(MessageSenderUtils.composeEmptyMessage(Messages.InGame.FromServer.GAME_START), null);
		}
		
		catch (IOException e) {
			System.out.println("send_GAME_START : Network Error : "+e.toString());
		}
	}
	
	public void send_SHOOT(UUID targetUUID, String weaponName, int damage){
		print("Sending SHOOT");
		
		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(
					Messages.InGame.FromServer.SHOOT,
					MessageSenderUtils.constructShootString(client_UUID, targetUUID, weaponName, damage)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_SHOOT : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_SHOOT : Network Error : "+e.toString());
		}
	}
	
	
	
	//LOBBY MESSAGES
	/////////////////////////////////////////////////////////

	
	
	public void send_CONFIGURATION(){
		print("Sending CONFIGURATION");
		try {
			sendMessage(MessageSenderUtils.constructConfigurationJsonString(client_Address));
		} 
		catch (IOException e) {
			System.out.println("send_CONFIGURATION : Network Error : "+e.toString());
		}
	}
	
	public static void send_PLAYER_JOINED(UUID client_UUID){
		System.out.println("Sending PLAYER_JOINED");
		
		try{
			Player player = GameManager.getPlayer(client_UUID);		
			broadcastMessage(
				MessageSenderUtils.composeMessage(Messages.Lobby.FromServer.PLAYER_JOINED, 
						MessageSenderUtils.constructPlayerJoinedString(
								client_UUID, 
								player.nickname, 
								player.professionTitle, 
								GameManager.getTeam(client_UUID)
						)
				),
				client_UUID
			);
		}
		catch(IOException e){
			System.out.println("send_PLAYER_JOINED : Network Error : "+e.toString());
			
		}
	}
	
	public static void send_PLAYER_LEFT(UUID client_UUID){
		System.out.println("Sending PLAYER_LEFT");
		
		try{
			broadcastMessage(
				MessageSenderUtils.composeMessage(
						Messages.Lobby.FromServer.PLAYER_LEFT, 
						MessageSenderUtils.constructPlayerLeftString(client_UUID)
				),
				client_UUID
			);
		}
		catch(IOException e){
			System.out.println("send_PLAYER_LEFT : Network Error : "+e.toString());			
		}
	}	
	
	public void send_CHANGE_NAME(String newName, UUID client_UUID){
		System.out.println("Sending CHANGE_NAME");		
		
		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(
						Messages.Lobby.FromServer.CHANGE_NAME, 
						MessageSenderUtils.constructChangeNameString(client_UUID, newName)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_NAME : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_CHANGE_NAME : Network Error : "+e.toString());
		}
		
	}
	
	public void send_CHANGE_TEAM(int newTeam){
		print("Sending CHANGE_TEAM");

		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(
						Messages.Lobby.FromServer.CHANGE_TEAM, 
						MessageSenderUtils.constructChangeTeamString(client_UUID, newTeam)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_TEAM : Json Error : "+e.toString());
		}
		
		catch (IOException e) {
			System.out.println("send_CHANGE_TEAM : Network Error : "+e.toString());
		}
	}
	
	public void send_CHANGE_PROFESSION(String newProfession){
		print("Sending CHANGE_PROFESSION");
		
		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(
						Messages.Lobby.FromServer.CHANGE_PROFESSION, 
						MessageSenderUtils.constructChangeProfessionString(client_UUID, newProfession)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_PROFESSION : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_CHANGE_PROFESSION : Network Error : "+e.toString());
		}		
	}
	
	public void send_MESSAGE_ALL(String message){
		print("Sending MESSAGE_ALL");
		
		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(
						Messages.Lobby.FromServer.MESSAGE_ALL, 
						MessageSenderUtils.constructMessageAll(client_UUID, message)
				),
				client_UUID
			);
		} 
		catch (JsonProcessingException e) {
			System.out.println("send_MESSAGE_ALL : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_MESSAGE_ALL : Network Error : "+e.toString());
		}
	}
	
	public static void send_ENTER_GAME(){
		System.out.println("Sending ENTER_GAME");
		
		try {
			broadcastMessage(MessageSenderUtils.composeEmptyMessage(Messages.Lobby.FromServer.ENTER_GAME), null);
		} 
		
		catch (IOException e) {
			System.out.println("send_ENTER_GAME : Network Error : "+e.toString());
		}
	}
	
	public void send_READY(boolean isPlayerReady){
		print("Sending READY");
		
		try {
			broadcastMessage(
				MessageSenderUtils.composeMessage(
						Messages.Lobby.FromServer.READY, 
						MessageSenderUtils.constructReadyString(client_UUID, isPlayerReady)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_READY : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_READY : Network Error : "+e.toString());
		}
	}
	
	public static void send_COUNTDOWN(int secondsLeft){
		System.out.println("Sending COUNTDOWN");
		
		try {			
			broadcastMessage(
					MessageSenderUtils.composeMessage(
							Messages.Lobby.FromServer.COUNTDOWN, 
							MessageSenderUtils.constructCountdownString(secondsLeft)
					), 
					null
			);
		}
		
		catch (JsonProcessingException e) {
			System.out.println("send_COUNTDOWN : Json Error : " + e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_COUNTDOWN : Network Error : " + e.toString());
		}
	}

	
	
	
	
	
	
	

	
	
	
}
