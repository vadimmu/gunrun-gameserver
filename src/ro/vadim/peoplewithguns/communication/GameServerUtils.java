package ro.vadim.peoplewithguns.communication;

import ro.vadim.peoplewithguns.gamesmanagement.ServerGameManager;
import ro.vadim.peoplewithguns.gamesmanagement.TcpServer;

public class GameServerUtils {

	private GameServer gameServer = null;
	
	
	public GameServerUtils(GameServer newGameServer){
		setGameServer(newGameServer);
	}	
	
	
    public void resetServer(){
        
        System.out.println("ALL PLAYERS HAVE LEFT. RESETTING SERVER...");                               
        getGameServer().getGameManager().game_entered = false;
        getGameServer().getGameManager().game_started = false;
        getGameServer().getConnectionManager().heartbeats.clear();
        getGameServer().getConnectionManager().connections.clear();
        getGameServer().getConnectionManager().messageReceiverList.clear();
        getGameServer().getConnectionManager().messageSenderList.clear();
        getGameServer().getGameManager().home_team.clear();
        getGameServer().getGameManager().away_team.clear();
        
    }
    
    

    public synchronized void closeServer(){    	   	
    	    	
    	resetServer();
    	
    	System.out.println("STOPPING SERVER...");
    	
    	ServerGameManager.removeGame(getGameServer().getServerID());
    	
        getGameServer().stopGameServer();
    }



                                        



	public GameServer getGameServer() {
		return gameServer;
	}
	
	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
}