package ro.vadim.peoplewithguns.communication.misc;

import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.GameManager;

public class CountdownToGame implements Runnable{		
	
	private static final int COUNTDOWN_TIME = 5;	
	
	int countdownSeconds = 5;
	
	boolean stop = false;
	
	private void sendCountdownToAll(int countdownSeconds){
		
		if(GameManager.areAllPlayersReady() == false)
			stop = true;
		else
			MessageSender.send_COUNTDOWN(countdownSeconds);		
	}
	
	private void sendStartGameToAll(){
		if(GameManager.areAllPlayersReady() == true)
			MessageSender.send_ENTER_GAME();
	}
	
	
	public void stop(){
		stop = true;
	}
	
	
	@Override
	public void run() {
		try {				
			countdownSeconds = COUNTDOWN_TIME;
			
			while((GameManager.isEverybodyReady() == true) && (countdownSeconds > 0) && (stop == false)){
				
				if(GameManager.isEverybodyReady() == false){
					stop = true;					
					System.out.println("PLAYERS ARE NOT READY !!!!!!!!!!!!!!!!!!!!!!!!!!");
				}
				
				
				Thread.sleep(1000);
				
				countdownSeconds--;
				
				System.out.println("COUNTDOWN : "+String.valueOf(countdownSeconds));
				sendCountdownToAll(countdownSeconds);
				
				
				if(countdownSeconds == 0){
					sendStartGameToAll();
				}
			}
			
			Thread.sleep(2000);
			MessageSender.send_GAME_START();
			
		}
		
		catch (InterruptedException e) {
			System.out.println("CountdownToGame : countdown error : " + e.toString());			
		}
		
	}
}
