package ro.vadim.peoplewithguns.communication;

public class Messages {
	
	public static class Admin{
		public static int HEARBEAT = 3004;
		
	}	
	
	public static class InGame{
		public static class ToServer{
			public static int CHANGE_POSITION = 0;		// {latitude: newLatitude, longitude: newLongitude}
			public static int SHOOT = 1; 				// {target: targetUUID, weapon: weapon_name, (optional)timestamp: timeStamp}
			public static int MESSAGE_TEAM = 2; 		// {message : messageString}			
		}
		
		public static class FromServer{
			public static int CHANGE_POSITION = 3;		// {player: playerUUID, latitude: newLatitude, longitude: newLongitude}
			public static int SHOOT = 4;				// {player: playerUUID, target: targetUUID, weapon: weapon_name, damage: damageAmount, (optional)timestamp: timeStamp}
			public static int MESSAGE_TEAM = 5;			// {player: playerUUID, message: newMessage}
			public static int GAME_START = 6;			// broadcast message that is sent from the server to let all the players know 
														// that they are sufficiently far from each other and their weapons are now considered active			
		}
		
	}
	
	public static class Lobby{
		public static class ToServer{
			public static int MESSAGE_ALL = 7;			// {message: messageString}
			public static int CHANGE_NAME = 8;			// {name: newName}		
			public static int CHANGE_TEAM = 9;			// {}		
			public static int CHANGE_PROFESSION = 10;	// {profession: newProfession}
			public static int CHOOSE_WEAPONS = 11;		// this will be made available in further versions where there will be more weapons from which to choose
			public static int READY = 12;				// {ready : true/false}
			
			
		}
		
		public static class FromServer{
			public static int ENTER_GAME = 13;			// {}
			public static int MESSAGE_ALL = 14;			// {player: playerUUID, message: messageString}
			public static int CHANGE_NAME = 15;			// {player: playerUUID, nickname: newName}
			public static int CHANGE_TEAM = 16;			// {player: playerUUID, team: newTeam}		
			public static int CHANGE_PROFESSION = 17;	// {player: playerUUID, profession: newProfession}
			public static int READY = 18;				// {player: playerUUID}
								
			public static int PLAYER_JOINED = 19;		// {player: playerUUID, name: playerName}
			public static int PLAYER_LEFT = 20;			// {player: playerUUID}
			
			public static int CONFIGURATION = 21;		// that huge JSON thing that you get from Config
			public static int COUNTDOWN = 22;			// {secondsLeft: number_of_seconds_left}
			
		}
	}
}
