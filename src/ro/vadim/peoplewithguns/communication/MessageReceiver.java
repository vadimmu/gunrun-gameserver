package ro.vadim.peoplewithguns.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.security.AllPermission;
import java.util.Date;
import java.util.EventListener;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.game.gameelements.GameManager;
import ro.vadim.peoplewithguns.game.gameelements.Player;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



///////////////////////////////////////////////////////////////SERVER
public class MessageReceiver implements Runnable{

	private Socket socket = null;
	
	private InputStream fromServer_Stream = null;
	private BufferedReader fromServer_Reader = null;	
	private MessageSender messageSender = null;
	private InetAddress clientAddress = null; 
	private UUID clientUUID = null;
	
	private boolean stop = false;
	
	public MessageReceiver(Socket socket, UUID playerUUID) {		
		this.socket = socket;
		clientAddress = socket.getInetAddress();
		clientUUID = playerUUID;
		ConnectionManager.addMessageReceiver(this, clientAddress);
	}
	
	private MessageSender getMessageSender(){
		InetAddress remoteAddress = this.socket.getInetAddress();		
		return ConnectionManager.messageSenderList.get(remoteAddress);
	}
	
	public Map<String, Object> decodeMessage(String message) throws JsonParseException, JsonMappingException, IOException{		
		
		Map<String,Object> jsonObject = MessageSenderUtils.mapper.readValue(message, Map.class);
		
		return jsonObject;
	}
	
	public void on(String messageType, Map<String, Object> jsonObject, MessageHandler messageManager){
		
		if(messageSender == null)
			messageSender = getMessageSender();
		
		if(!jsonObject.containsKey("messageType")){
			System.out.println("the Json object passed as an argument does not have a \"messageType\" field.");			
			return;	
		}
			
		
		if(jsonObject.get("messageType").equals(messageType)){			
			
			if(jsonObject.containsKey("data")){				
				messageManager.manageMessage((Map<String, Object>) jsonObject.get("data"));
			}
			else{
				System.out.println("correct message type, but no data...");
			}
		}
	}
	
	public void print(String message){
		System.out.println(message);
	}
	
	public void handleMessages(Socket socket){
		
		try {
			
			fromServer_Stream = socket.getInputStream();
			fromServer_Reader = new BufferedReader(new InputStreamReader(fromServer_Stream));
			
			String receivedMessage = "";
		
		
			while(((receivedMessage = fromServer_Reader.readLine()) != null) && (stop == false)){
			
				
				Map<String, Object> messageObject = decodeMessage(receivedMessage);
				
				 
				
				//IN-GAME MESSAGES
				///////////////////////////////////////////////////////////////////////////////
				int CHANGE_POSITION = 0;	// {latitude: newLatitude, longitude: newLongitude}
				int SHOOT = 1; 				// {target: targetUUID, weapon: weapon_name, damage: weapon_damage, (optional)timestamp: timeStamp}
				int MESSAGE_TEAM = 2; 		// {message : messageString}	
				
				
				
				on(String.valueOf(Messages.InGame.ToServer.CHANGE_POSITION), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						print("received CHANGE_POSITION");
						
						double latitude = 0;
						double longitude = 0;
						if((jsonData.containsKey("latitude"))&&(jsonData.containsKey("longitude"))){
							
							latitude = Double.valueOf((String)jsonData.get("latitude"));
							longitude = Double.valueOf((String)jsonData.get("longitude"));
							
							
							print("sending from server : CHANGE_POSITION");
							messageSender.send_CHANGE_POSITION(latitude, longitude);
							
						}
						
					}
				});
				
				on(String.valueOf(Messages.InGame.ToServer.SHOOT), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						UUID targetUUID = null;
						String weaponName = null;
						int damage = 0;
						
						print("received SHOOT");
						
						if((jsonData.containsKey("targetUUID"))&&(jsonData.containsKey("weapon"))&&(jsonData.containsKey("damage"))){
							
							targetUUID = UUID.fromString((String) jsonData.get("targetUUID"));
							weaponName = (String) jsonData.get("weapon");
							damage = Integer.valueOf((String) jsonData.get("damage"));
														
							messageSender.send_SHOOT(targetUUID, weaponName, damage);
						}
					}
				});

				on(String.valueOf(Messages.InGame.ToServer.MESSAGE_TEAM), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String message = "";
						print("received MESSAGE_TEAM");
						
						if(jsonData.containsKey("message")){
							
							message = (String) jsonData.get("message");
							
							
							messageSender.send_MESSAGE_ALL(message);							
						}
					}
				});
				
				
				//LOBBY MESSAGES
				///////////////////////////////////////////////////////////////////////////////
				int MESSAGE_ALL = 7;		// {message: messageString}
				int CHANGE_NAME = 8;		// {name: newName}		
				int CHANGE_TEAM = 9;		// {}		
				int CHANGE_PROFESSION = 10;	// {profession: newProfession}
				int CHOOSE_WEAPONS = 11;	// this will be made available in further versions where there will be more weapons from which to choose
				int READY = 12;				// {}
							
				on(String.valueOf(Messages.Lobby.ToServer.MESSAGE_ALL), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String message = "";
						
						print("received MESSAGE_ALL");
						
						if(jsonData.containsKey("message")){
							
							message = (String) jsonData.get("message");
							
							
							messageSender.send_MESSAGE_ALL(message);
							
						}
						
					}
				});
				
				on(String.valueOf(Messages.Lobby.ToServer.CHANGE_NAME), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String newName = "";
						
						print("received CHANGE_NAME");
						
						if(jsonData.containsKey("name")){
							
							print("changing name and broadcasting the name change");
							newName = (String) jsonData.get("name");
							
							GameManager.changePlayerName(clientUUID, newName);
														
							messageSender.send_CHANGE_NAME(newName, clientUUID);						
						}					
					}
				});	
								
				on(String.valueOf(Messages.Lobby.ToServer.CHANGE_TEAM), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						
						print("received CHANGE_TEAM");						
												
						
						int team = GameManager.getTeam(ConnectionManager.getUuidByAddress(clientAddress));
							
						GameManager.movePlayer(clientUUID);			
					
						
						
						System.out.println("TEAMS : ");
						System.out.println("==================");
						System.out.println("HOME TEAM");
						for(Entry e : GameManager.home_team.entrySet()){							
							System.out.println(((Player)e.getValue()).nickname+" ("+((UUID)e.getKey()).toString()+")");
						}
						
						System.out.println("AWAY TEAM");
						for(Entry e : GameManager.away_team.entrySet()){							
							System.out.println(((Player)e.getValue()).nickname+" ("+((UUID)e.getKey()).toString()+")");
						}
						System.out.println("==================");
						
						
						if(team == GameManager.HOME_TEAM)
							messageSender.send_CHANGE_TEAM(GameManager.AWAY_TEAM);
						
						else if(team == GameManager.AWAY_TEAM)
							messageSender.send_CHANGE_TEAM(GameManager.HOME_TEAM);
						
						print("broadcast CHANGE_TEAM message");
					}
				});	
				
				on(String.valueOf(Messages.Lobby.ToServer.CHANGE_PROFESSION), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String newProfession = "";
						
						print("received CHANGE_PROFESSION");
						
						if(jsonData.containsKey("profession")){
							
							newProfession = (String) jsonData.get("profession");
														
							GameManager.changePlayerProfession(clientUUID, newProfession);
							
							
							messageSender.send_CHANGE_PROFESSION(newProfession);							
						}						
					}
				});	
				
				on(String.valueOf(Messages.Lobby.ToServer.CHOOSE_WEAPONS), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						//TODO implement this when it's the case...
						print("received CHOOSE_WEAPONS");
						
					}
				});	
				
				on(String.valueOf(Messages.Lobby.ToServer.READY), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						
						print("received READY");
						
						if(jsonData.containsKey("ready")){
														
							boolean isPlayerReady = Boolean.valueOf(String.valueOf(jsonData.get("ready")));
							
							GameManager.setPlayerReady(clientUUID, isPlayerReady);
							
							
							messageSender.send_READY(isPlayerReady);
							System.out.println("ARE ALL PLAYERS READY ?    " + String.valueOf(GameManager.areAllPlayersReady()));
							
							if(GameManager.areAllPlayersReady() == true){
								
								System.out.println("STARTING COUNTDOWN !");
								GameManager.startCountdown();
							}
						}
					}
				});	
				
				on(String.valueOf(Messages.Admin.HEARBEAT), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						
						//print("received HEARTBEAT");						
						Date lastHeartbeat = new Date();
						ConnectionManager.setLastHeartbeatTime(clientAddress, lastHeartbeat);						
					}
				});	
				
				
			}
			
			
		} 
		
		catch (JsonParseException e) {
			System.out.println("MessageReceiver : "+e.toString());
		} 
		
		catch (JsonMappingException e) {
			System.out.println("MessageReceiver : "+e.toString());
		} 
		
		catch (IOException e) {			
			System.out.println("MessageReceiver : "+e.toString());			
		}		
		
		
	}
	
	
	@Override
	public void run() {
		handleMessages(socket);			
	}
	
	public void stop(){
		try{
			stop = true;
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
		}
		catch(IOException e){
			System.out.println("MessageReceiver(Stop) : could not close connection properly.");
			System.out.println("MessageReceiver(Stop) : "+e.toString());
			
		}
	}

}
