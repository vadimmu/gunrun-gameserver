package ro.vadim.peoplewithguns.communication.receiver;

import java.net.InetSocketAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;
import java.util.Map.Entry;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.game.GameManager;

public class HeartbeatListener implements Runnable{
	
	public final static long HEARTBEAT_TIMEOUT = 15000;
	public boolean stop = false;
	private GameServer gameServer = null;
	
	
	public HeartbeatListener(GameServer newGameServer){
		setGameServer(newGameServer);
	}
	
	
	
	
	private void resetServer(){
		
		System.out.println("ALL PLAYERS HAVE LEFT. RESETTING SERVER...");				
		getGameServer().getGameManager().game_entered = false;
		getGameServer().getGameManager().game_started = false;
	}
	
	
	private void closeServer(){				
		getGameServer().stopGameServer();
	}
	
	
	private synchronized void checkHeartbeats(){
		
		try {
			Thread.sleep(1000);
			Date now = new Date();
						
			//if all the players are out of the game, reset the server
			
			if((getGameServer().getConnectionManager().connections.size() == 0) && ((getGameServer().getGameManager().game_entered == true) || (getGameServer().getGameManager().game_started == true))){
				
				
				resetServer();				
				//closeServer();
				
			}
			
			//routinely check for the presence of the players
			if(getGameServer().getConnectionManager().connections.isEmpty()){
				
				getGameServer().getConnectionManager().heartbeats.clear();
				getGameServer().getConnectionManager().connections.clear();
				getGameServer().getConnectionManager().messageReceiverList.clear();
				getGameServer().getConnectionManager().messageSenderList.clear();
				getGameServer().getGameManager().home_team.clear();
				getGameServer().getGameManager().away_team.clear();
			}
			
			HashMap <InetSocketAddress, Date> heartbeatsCopy = new HashMap<InetSocketAddress, Date>(getGameServer().getConnectionManager().heartbeats);									
			
			for(Entry e : heartbeatsCopy.entrySet()){
				
				InetSocketAddress clientAddress = (InetSocketAddress) e.getKey();				
				Date lastHeartbeat = (Date) e.getValue();
				
				UUID playerUUID = getGameServer().getConnectionManager().getUuidByAddress(clientAddress);
								
				if((now.getTime() - lastHeartbeat.getTime()) >= HEARTBEAT_TIMEOUT){
					System.out.println("The heartbeat interval for player "+playerUUID.toString()+" has expired");
					System.out.println("Removing player "+playerUUID.toString());
					
					if(getGameServer().getConnectionManager().getAddressByUUID(playerUUID) != null)					
						getGameServer().getConnectionManager().closeConnection(clientAddress);						
				}
			}
		}			
		
		catch (InterruptedException e) {				
			System.out.println("Heartbeat Listener stopped working ! : "+e.toString());
		}		
	}
	
	
	@Override
	public void run() {
		
		while(stop == false){
			checkHeartbeats();				
		}
	}



	public GameServer getGameServer() {
		return gameServer;
	}



	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
}
