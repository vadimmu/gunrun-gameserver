package ro.vadim.peoplewithguns.communication.receiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.AllPermission;
import java.util.Date;
import java.util.EventListener;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.MessageHandler;
import ro.vadim.peoplewithguns.communication.messages.Messages;
import ro.vadim.peoplewithguns.communication.messages.Messages.Admin;
import ro.vadim.peoplewithguns.communication.messages.Messages.InGame;
import ro.vadim.peoplewithguns.communication.messages.Messages.Lobby;
import ro.vadim.peoplewithguns.communication.messages.Messages.InGame.ToServer;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.communication.sender.MessageSenderUtils;
import ro.vadim.peoplewithguns.game.GameManager;
import ro.vadim.peoplewithguns.game.Player;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



///////////////////////////////////////////////////////////////SERVER
public class MessageReceiver implements Runnable{

	private GameServer gameServer = null;
	
	
	private Socket socket = null;
	
	private InputStream fromServer_Stream = null;
	private BufferedReader fromServer_Reader = null;	
	private MessageSender messageSender = null;
	private InetSocketAddress clientAddress = null; 
	private UUID clientUUID = null;
	
	
	public boolean stop = false;
	
	public MessageReceiver(GameServer newGameServer, Socket socket, UUID playerUUID) {
		this.socket = socket;
		
		setGameServer(newGameServer);
		
		clientAddress = new InetSocketAddress(socket.getInetAddress(), socket.getPort());
		clientUUID = playerUUID;
		getGameServer().getConnectionManager().addMessageReceiver(this, clientAddress);				
	}
	
	private MessageSender getMessageSender(){
		InetSocketAddress remoteAddress = new InetSocketAddress(socket.getInetAddress(), socket.getPort());		
		return getGameServer().getConnectionManager().messageSenderList.get(remoteAddress);
	}
	
	public Map<String, Object> decodeMessage(String message) throws JsonParseException, JsonMappingException, IOException{		
		
		Map<String,Object> jsonObject = getGameServer().getMessageSenderUtils().mapper.readValue(message, Map.class);
		
		return jsonObject;
	}
	
	public void on(String messageType, Map<String, Object> jsonObject, MessageHandler messageManager){
		
		if(messageSender == null)
			messageSender = getMessageSender();
		
		if(!jsonObject.containsKey("messageType")){
			System.out.println("the Json object passed as an argument does not have a \"messageType\" field.");			
			return;	
		}
			
		
		if(jsonObject.get("messageType").equals(messageType)){			
			
			if(jsonObject.containsKey("data")){				
				messageManager.manageMessage((Map<String, Object>) jsonObject.get("data"));
			}
			else{
				System.out.println("correct message type, but no data...");
			}
		}
	}
	
	public void print(String message){
		System.out.println(message);
	}
	
	public void handleMessages(Socket socket){
		
		try {
			
			fromServer_Stream = socket.getInputStream();
			fromServer_Reader = new BufferedReader(new InputStreamReader(fromServer_Stream));
			
			String receivedMessage = "";
		
		
			while(((receivedMessage = fromServer_Reader.readLine()) != null) && (stop == false)){
			
				
				Map<String, Object> messageObject = decodeMessage(receivedMessage);
							
				
				on(String.valueOf(Messages.InGame.ToServer.CHANGE_POSITION), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						print("received CHANGE_POSITION");
						
						double latitude = 0;
						double longitude = 0;
						
						if((jsonData.containsKey("latitude"))&&(jsonData.containsKey("longitude"))){
							
							latitude = Double.valueOf((String)jsonData.get("latitude"));
							longitude = Double.valueOf((String)jsonData.get("longitude"));
							
							double previousLatitude = 0;
							double previousLongitude = 0;
							
							if(getGameServer().getGameManager().getPlayer(clientUUID).latitude != null)							
								previousLatitude = getGameServer().getGameManager().getPlayer(clientUUID).latitude;
							
							if(getGameServer().getGameManager().getPlayer(clientUUID).longitude != null)
								previousLongitude = getGameServer().getGameManager().getPlayer(clientUUID).longitude;
							
							
							if((latitude!=previousLatitude) || (longitude != previousLongitude)){
							
								getGameServer().getGameManager().setPlayerPosition(clientUUID, latitude, longitude);
								
								print("sending from server : CHANGE_POSITION");
								
								MessageSender.send_CHANGE_POSITION(getGameServer(), clientUUID, latitude, longitude);
								
								if((getGameServer().getGameManager().game_entered == true) && (getGameServer().getGameManager().game_started == false)){
									if(getGameServer().getGameManager().isGameStartConditionMet() == true){
										MessageSender.send_GAME_START(getGameServer());
										getGameServer().getGameManager().game_started = true;
									}
								}
							}
						}
					}
				});
				
				on(String.valueOf(Messages.InGame.ToServer.SHOOT), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						UUID targetUUID = null;
						String weaponName = null;
						int damage = 0;
						
						print("received SHOOT");
						
						if((jsonData.containsKey("targetUUID"))&&(jsonData.containsKey("weapon"))&&(jsonData.containsKey("damage"))){
							
							targetUUID = UUID.fromString((String) jsonData.get("targetUUID"));
							weaponName = (String) jsonData.get("weapon");
							damage = Integer.valueOf((String) jsonData.get("damage"));
														
							messageSender.send_SHOOT(targetUUID, weaponName, damage);
						}
					}
				});

				on(String.valueOf(Messages.InGame.ToServer.MESSAGE_TEAM), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String message = "";
						print("received MESSAGE_TEAM");
						
						if(jsonData.containsKey("message")){
							
							message = (String) jsonData.get("message");							
							messageSender.send_MESSAGE_TEAM(clientUUID, message);							
						}
					}
				});
											
				on(String.valueOf(Messages.Lobby.ToServer.MESSAGE_ALL), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String message = "";
						
						System.out.println("received MESSAGE_ALL");
						
						if(jsonData.containsKey("message")){
							
							message = (String) jsonData.get("message");
							
							MessageSender.send_MESSAGE_ALL(getGameServer(), clientUUID, message);							
						}						
					}
				});
				
				on(String.valueOf(Messages.Lobby.ToServer.CHANGE_NAME), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String newName = "";
						
						print("received CHANGE_NAME");
						
						if(jsonData.containsKey("name")){
							
							print("changing name and broadcasting the name change");
							newName = (String) jsonData.get("name");
							
							getGameServer().getGameManager().setPlayerName(clientUUID, newName);
														
							messageSender.send_CHANGE_NAME(getGameServer(), clientUUID, newName);						
						}					
					}
				});	
								
				on(String.valueOf(Messages.Lobby.ToServer.CHANGE_TEAM), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						
						print("received CHANGE_TEAM");						
												
						
						int team = getGameServer().getGameManager().getTeam(getGameServer().getConnectionManager().getUuidByAddress(clientAddress));
							
						getGameServer().getGameManager().movePlayer(clientUUID);			
					
						
						
						System.out.println("TEAMS : ");
						System.out.println("==================");
						System.out.println("HOME TEAM");
						for(Entry e : getGameServer().getGameManager().home_team.entrySet()){							
							System.out.println(((Player)e.getValue()).nickname+" ("+((UUID)e.getKey()).toString()+")");
						}
						
						System.out.println("AWAY TEAM");
						for(Entry e : getGameServer().getGameManager().away_team.entrySet()){							
							System.out.println(((Player)e.getValue()).nickname+" ("+((UUID)e.getKey()).toString()+")");
						}
						System.out.println("==================");
						
						
						if(team == GameManager.HOME_TEAM)
							messageSender.send_CHANGE_TEAM(getGameServer(), clientUUID, GameManager.AWAY_TEAM);
						
						else if(team == GameManager.AWAY_TEAM)
							messageSender.send_CHANGE_TEAM(getGameServer(), clientUUID, GameManager.HOME_TEAM);
						
						print("broadcast CHANGE_TEAM message");
					}
				});	
				
				on(String.valueOf(Messages.Lobby.ToServer.CHANGE_PROFESSION), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						String newProfession = "";
						
						print("received CHANGE_PROFESSION");
						
						if(jsonData.containsKey("profession")){
							
							newProfession = (String) jsonData.get("profession");
														
							getGameServer().getGameManager().setPlayerProfession(clientUUID, newProfession);
														
							messageSender.send_CHANGE_PROFESSION(newProfession);							
						}						
					}
				});	
				
				on(String.valueOf(Messages.Lobby.ToServer.CHOOSE_WEAPONS), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						//TODO implement this when it's the case...
						print("received CHOOSE_WEAPONS");
						
					}
				});	
				
				on(String.valueOf(Messages.Lobby.ToServer.READY), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
												
						if(jsonData.containsKey("ready")){
														
							boolean isPlayerReady = Boolean.valueOf(String.valueOf(jsonData.get("ready")));
							
							print("received READY : " + String.valueOf(isPlayerReady));
							
							getGameServer().getGameManager().setPlayerReady(clientUUID, isPlayerReady);
														
							messageSender.send_READY(getGameServer(), clientUUID, isPlayerReady);
							System.out.println("ARE ALL PLAYERS READY ?    " + String.valueOf(getGameServer().getGameManager().areAllPlayersReady()));
							
							if(getGameServer().getGameManager().isEverybodyReady() == true){			
								System.out.println("STARTING COUNTDOWN !");
								getGameServer().getGameManager().startCountdown();
							}
							else{
								getGameServer().getGameManager().stopCountdown();								
							}
						}
					}
				});	
				
				on(String.valueOf(Messages.Admin.ToServer.HEARTBEAT), messageObject, new MessageHandler() {
					
					@Override
					public void manageMessage(Map<String, Object> jsonData) {
						
						//print("received HEARTBEAT");						
						Date lastHeartbeat = new Date();
						getGameServer().getConnectionManager().setLastHeartbeatTime(clientAddress, lastHeartbeat);						
					}
				});	
				
				
			}
			
			if(receivedMessage == null){
				if(stop != true){
					System.out.println("RECEIVER STOPPED !!! received message = null");
					System.out.println("Closing connection to player " + clientUUID.toString());
				}
				
				else{
					System.out.println("RECEIVER STOPPED !!! stop = true !");
					stop = false;
				}
				
				getGameServer().getConnectionManager().closeConnection(clientAddress);
			}
			
		} 
		
		catch (JsonParseException e) {
			System.out.println("MessageReceiver : "+e.toString());
		} 
		
		catch (JsonMappingException e) {
			System.out.println("MessageReceiver : "+e.toString());
		} 
		
		catch (IOException e) {			
			System.out.println("MessageReceiver : "+e.toString());			
		}		
		
		
	}
	
	
	@Override
	public void run() {
		handleMessages(socket);			
	}
	
	public void stop(){
		
		try{
			stop = true;			
			socket.shutdownInput();
			socket.shutdownOutput();
			socket.close();
			
		}
		catch(Exception e){
			System.out.println("MessageReceiver(Stop) : could not close connection properly.");
			System.out.println("MessageReceiver(Stop) : "+e.toString());
			System.out.println("MessageReceiver(Stop) : removing player");
			getGameServer().getConnectionManager().closeConnection(clientAddress); ///////////// PROBLEME AICI !
		}
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}

}
