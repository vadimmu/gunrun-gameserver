package ro.vadim.peoplewithguns.communication;

import ro.vadim.peoplewithguns.game.gameelements.GameManager;

public class CountdownToGame implements Runnable{		
	
	private static final int COUNTDOWN_TIME = 5;	
	
	int countdownSeconds = 5;
	
	boolean stop = false;
	
	private void sendCountdownToAll(int countdownSeconds){
		
		MessageSender.send_COUNTDOWN(countdownSeconds);		
	}
	
	private void sendStartGameToAll(){
		
		MessageSender.send_ENTER_GAME();
	}
	
	
	public void stop(){
		stop = true;
	}
	
	
	@Override
	public void run() {
		try {				
			countdownSeconds = COUNTDOWN_TIME;
			
			while((GameManager.areAllPlayersReady() == true) && (countdownSeconds > 0) && (stop == false)){
				
				Thread.sleep(1000);
				
				countdownSeconds--;
				System.out.println("COUNTDOWN : "+String.valueOf(countdownSeconds));
				sendCountdownToAll(countdownSeconds);
				
				if(countdownSeconds == 0){
					sendStartGameToAll();
				}					
			}
			
			Thread.sleep(1000);
			MessageSender.send_GAME_START();
			
		}
		
		catch (InterruptedException e) {
			System.out.println("CountdownToGame : countdown error : " + e.toString());
		}
		
	}
}
