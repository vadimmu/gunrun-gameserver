package ro.vadim.peoplewithguns.communication;

import java.net.InetAddress;
import java.util.Date;
import java.util.UUID;
import java.util.Map.Entry;

import ro.vadim.peoplewithguns.game.gameelements.GameManager;

public class HeartbeatListener implements Runnable{
	
	public static long HEARTBEAT_TIMEOUT = 3000;
	public static boolean stop = false;
	
	private synchronized void checkHeartbeats(){
		
		try {
			Thread.sleep(1000);
			Date now = new Date();
			
			for(Entry e : ConnectionManager.heartbeats.entrySet()){
				
				InetAddress clientAddress = (InetAddress) e.getKey();				
				Date lastHeartbeat = (Date) e.getValue();
				
				UUID playerUUID = ConnectionManager.getUuidByAddress(clientAddress);
				
				
				
				if((now.getTime() - lastHeartbeat.getTime()) >= HEARTBEAT_TIMEOUT){
					ConnectionManager.closeConnection(clientAddress);						
				}					
			}
		}			
		
		catch (InterruptedException e) {				
			System.out.println("Heartbeat Listener stopped working ! : "+e.toString());
			//stop = true;
		}		
	}
	
	
	@Override
	public void run() {
		
		while(stop == false){
			checkHeartbeats();				
		}
	}
}
