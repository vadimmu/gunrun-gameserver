package ro.vadim.peoplewithguns.communication;

import java.util.Map;

public interface MessageHandler {
	
	public void manageMessage(Map<String, Object> jsonData);
	
}