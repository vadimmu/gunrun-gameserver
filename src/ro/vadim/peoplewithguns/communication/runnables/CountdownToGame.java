package ro.vadim.peoplewithguns.communication.runnables;

import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.GameManager;

public class CountdownToGame implements Runnable{		
	
	private GameServer gameServer = null;
	
	
	private static final int COUNTDOWN_TIME = 5;
	int countdownSeconds = 5;
	boolean stop = false;
	
	
	
	public CountdownToGame(GameServer newGameServer){
		
		setGameServer(newGameServer);
		
	}
	
	
	
	
	
	
	private void sendCountdownToAll(int countdownSeconds){
		
		if(getGameServer().getGameManager().areAllPlayersReady() == false)
			stop = true;
		else
			MessageSender.send_COUNTDOWN(getGameServer(), countdownSeconds);		
	}
	
	private void sendStartGameToAll(){
		if(getGameServer().getGameManager().areAllPlayersReady() == true)
			MessageSender.send_ENTER_GAME(getGameServer());
	}
	
	
	public void stop(){
		stop = true;
	}
	
	
	@Override
	public void run() {
		try {				
			countdownSeconds = COUNTDOWN_TIME;
			
			while((getGameServer().getGameManager().isEverybodyReady() == true) && (countdownSeconds > 0) && (stop == false)){
				
				if(getGameServer().getGameManager().isEverybodyReady() == false){
					stop = true;					
					System.out.println("PLAYERS ARE NOT READY !!!!!!!!!!!!!!!!!!!!!!!!!!");
				}
				
				
				Thread.sleep(1000);
				
				countdownSeconds--;
				
				System.out.println("COUNTDOWN : "+String.valueOf(countdownSeconds));
				sendCountdownToAll(countdownSeconds);
				
				
				if(countdownSeconds == 0){
					
					if((getGameServer().getGameManager().home_team.size() == 0)&&(getGameServer().getGameManager().away_team.size() == 0))
						return;
					
					sendStartGameToAll();
				}
			}
		}
		
		catch (InterruptedException e) {
			System.out.println("CountdownToGame : countdown error : " + e.toString());			
		}
		
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
}
