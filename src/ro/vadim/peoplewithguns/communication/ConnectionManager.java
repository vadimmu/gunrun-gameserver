package ro.vadim.peoplewithguns.communication;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.receiver.MessageReceiver;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;
import ro.vadim.peoplewithguns.game.GameManager;

public class ConnectionManager {

	public final int EXPECTED_CONNECTIONS = 50;	
	public HashMap<InetSocketAddress, UUID> connections = new HashMap<InetSocketAddress, UUID>(EXPECTED_CONNECTIONS);
	public HashMap<InetSocketAddress, Date> heartbeats = new HashMap<InetSocketAddress, Date>(EXPECTED_CONNECTIONS);
		
	public HashMap<InetSocketAddress, MessageReceiver> messageReceiverList = new HashMap<InetSocketAddress, MessageReceiver>(EXPECTED_CONNECTIONS);
	public HashMap<InetSocketAddress, MessageSender> messageSenderList = new HashMap<InetSocketAddress, MessageSender>(EXPECTED_CONNECTIONS);
	
	private GameServer gameServer = null;
	
	
	
	
	public ConnectionManager(GameServer newGameServer){
		setGameServer(newGameServer);
	}
	
	
	
	
	
	
	
	
	public synchronized MessageSender getMessageSender(InetSocketAddress address){
		if(messageSenderList.containsKey(address))
			return messageSenderList.get(address);
		
		return null;
	}	
	public synchronized void removeMessageSender(InetSocketAddress address) throws IOException{
		if(messageSenderList.containsKey(address)){			
			System.out.println("closeConnection : closing MessageSender");
			messageSenderList.get(address).stop();
			System.out.println("closeConnection : removing MessageSender");
			messageSenderList.remove(address);
		}
	}	
	public synchronized void addMessageSender(MessageSender sender, InetSocketAddress address){
		messageSenderList.put(address, sender);		
	}
	
	
	
	public synchronized MessageReceiver getMessageReceiver(InetSocketAddress address){
		if(messageReceiverList.containsKey(address))
			return messageReceiverList.get(address);
		
		return null;
	}
	public synchronized void removeMessageReceiver(InetSocketAddress address){
		if(messageReceiverList.containsKey(address)){			
			System.out.println("closeConnection : closing MessageReceiver");
			
			MessageReceiver ms = messageReceiverList.get(address);
			
			if(ms.stop == false){
				ms.stop();				
			}
			
			System.out.println("closeConnection : removing MessageReceiver");
			messageReceiverList.remove(address);
		}
		
	}
	public synchronized void addMessageReceiver(MessageReceiver receiver, InetSocketAddress address){
		messageReceiverList.put(address, receiver);
	}
	
	
	
	public synchronized void removeConnection(InetSocketAddress address){
		if(connections.containsKey(address)){
			System.out.println("closeConnection : removing connection record");
			connections.remove(address);
		}
	}	
	public synchronized void addConnection(Socket clientSocket, UUID clientUUID){		
		InetSocketAddress newClientAddress = new InetSocketAddress(clientSocket.getInetAddress(), clientSocket.getPort());		
		connections.put(newClientAddress, clientUUID);
	}	
	public synchronized UUID getUuidByAddress(InetSocketAddress address){
		if(connections.containsKey(address))
			return connections.get(address);
		
		return null;
	}
	
	public synchronized InetSocketAddress getAddressByUUID(UUID playerID){
		
		for(Entry<InetSocketAddress, UUID> e : connections.entrySet()){
			if(e.getValue().equals(playerID)){
				return e.getKey();			
			}
		}
		
		return null;
	}
	
	
	public synchronized void setLastHeartbeatTime(InetSocketAddress address, Date date){
		heartbeats.put(address, date);
	}
	public synchronized void removeHeartbeat(InetSocketAddress address){
		if(heartbeats.containsKey(address)){
			System.out.println("closeConnection : removing heartbeat record");
			heartbeats.remove(address);
		}
	}
	public synchronized Set<Entry<InetSocketAddress, Date>> getHeartbeatEntrySet(){
		return heartbeats.entrySet();
	}
	
		
	public synchronized void closeConnection(InetSocketAddress clientAddress){
		
		System.out.println("closeConnection : attempting to close the connection");
		
		try{
			
			System.out.println("closeConnection : deleting player");
			
			UUID playerUUID =  getUuidByAddress(clientAddress);
			
			
			if((getGameServer().getGameManager().home_team.containsKey(playerUUID)) || (getGameServer().getGameManager().away_team.containsKey(playerUUID))){
				MessageSender.send_PLAYER_LEFT(getGameServer(), playerUUID);
				getGameServer().getGameManager().deletePlayer(playerUUID);
			}
						
			removeHeartbeat(clientAddress);
			removeMessageSender(clientAddress);
			removeMessageReceiver(clientAddress);
			removeConnection(clientAddress);	
			
			
			if(getGameServer().getConnectionManager().connections.isEmpty()){
				//getGameServer().getGameServerUtils().resetServer();
				getGameServer().getGameServerUtils().closeServer();
            }
		}
		
		catch(IOException e){
			System.out.println("COULD NOT CLOSE THE CONNECTION");
			System.out.println(e.toString());			
		}
	}
	
	public synchronized HashMap<InetSocketAddress, UUID> getConnections(){
		return connections;
	}
	
	public synchronized void removeAllConnections(){
		connections.clear();		
	}
	
	public synchronized void closeAllConnections(){
					
			try{
				for(InetSocketAddress a : getConnections().keySet()){
				
					System.out.println("closeConnection : deleting player");
					
					UUID playerUUID =  getUuidByAddress(a);
					
					getGameServer().getGameManager().deletePlayer(playerUUID);
					
					removeHeartbeat(a);
					removeMessageSender(a);
					removeMessageReceiver(a);
					MessageSender.send_PLAYER_LEFT(getGameServer(), playerUUID);
				}				
				connections.clear();				
			}
			
			catch(IOException e){
				System.out.println("COULD NOT CLOSE THE CONNECTION");
				System.out.println(e.toString());			
			}
	}









	public GameServer getGameServer() {
		return gameServer;
	}









	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
}
