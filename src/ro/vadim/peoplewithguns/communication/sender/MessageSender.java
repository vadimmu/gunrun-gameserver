package ro.vadim.peoplewithguns.communication.sender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.Messages;
import ro.vadim.peoplewithguns.communication.messages.Messages.InGame;
import ro.vadim.peoplewithguns.communication.messages.Messages.Lobby;
import ro.vadim.peoplewithguns.communication.messages.Messages.Lobby.FromServer;
import ro.vadim.peoplewithguns.game.GameManager;
import ro.vadim.peoplewithguns.game.Player;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



/////////////////////////////////////////////////////// SERVER 
public class MessageSender{
		
	private GameServer gameServer = null;
	
	
	
	private OutputStream toClient_Stream = null;
	private BufferedWriter toClient_Writer = null;	
	private InetSocketAddress client_Address = null;
	private UUID client_UUID = null;
	
	
		
	public void stop(){
		
		try{
			toClient_Writer.flush();
			toClient_Writer.close();
		}
		
		catch(IOException e){			
			System.out.println("Cannot stop MessageSender for UUID: "+client_UUID.toString());			
		}
	}
		
	public MessageSender(Socket socket, GameServer newGameServer){
		
		setGameServer(newGameServer);
		
		try{
			toClient_Stream = socket.getOutputStream();
			toClient_Writer = new BufferedWriter(new OutputStreamWriter(toClient_Stream));
			client_Address = new InetSocketAddress(socket.getInetAddress(), socket.getPort());
			client_UUID = getGameServer().getConnectionManager().getUuidByAddress(client_Address);
			
			getGameServer().getConnectionManager().addMessageSender(this, client_Address);
		
			send_CONFIGURATION();
		}
		catch(IOException e){
			System.out.println("MessageSender(constructor) Error : "+e.toString());			
		}
		
	}	
	
	
	
	public static void selectiveMulticastMessage(GameServer gameServer, String message, ArrayList<UUID> recipients){
		
		ArrayList<UUID> keySet = new ArrayList<UUID>(gameServer.getGameManager().away_team.keySet());
		
		for(UUID teamMemberUUID : keySet){
			
			if(recipients.contains(teamMemberUUID)){
			
				InetSocketAddress teamMemberAddress = gameServer.getConnectionManager().getAddressByUUID(teamMemberUUID);			
				
				System.out.println("Sending message to "+gameServer.getGameManager().getPlayer(teamMemberUUID).nickname);			
				
				gameServer.getConnectionManager().messageSenderList.get(teamMemberAddress).sendMessage(message);
			}
		}
		
		for(UUID teamMemberUUID : keySet){
			
			if(recipients.contains(teamMemberUUID)){
				InetSocketAddress teamMemberAddress = gameServer.getConnectionManager().getAddressByUUID(teamMemberUUID);
				
				System.out.println("Sending message to "+gameServer.getGameManager().getPlayer(teamMemberUUID).nickname);
				
				gameServer.getConnectionManager().messageSenderList.get(teamMemberAddress).sendMessage(message);				
			}
		}
		
		
	}
	
	public static void multicastMessage(GameServer gameServer, String message, int team, UUID senderUUID) throws IOException{
		
		if(senderUUID == null)
			return;
		
		System.out.println("MULTICASTING MESSAGE !");
				
		if(team == gameServer.getGameManager().AWAY_TEAM){
			
			System.out.println("MULTICASTING MESSAGE to AWAY TEAM!");
			
			ArrayList<UUID> keySet = new ArrayList<UUID>(gameServer.getGameManager().away_team.keySet());
					
			for(UUID teamMemberUUID : keySet){
				
				if(! teamMemberUUID.equals(senderUUID)){
				
					InetSocketAddress teamMemberAddress = gameServer.getConnectionManager().getAddressByUUID(teamMemberUUID);
				
					System.out.println("Sending message to "+gameServer.getGameManager().getPlayer(teamMemberUUID).nickname);
				
					gameServer.getConnectionManager().messageSenderList.get(teamMemberAddress).sendMessage(message);
				}
			}
		}
		
		if(team == gameServer.getGameManager().HOME_TEAM){
			
			System.out.println("MULTICASTING MESSAGE to HOME TEAM!");
			
			ArrayList<UUID> keySet = new ArrayList<UUID>(gameServer.getGameManager().home_team.keySet());
					
			for(UUID teamMemberUUID : keySet){
				
				if(! teamMemberUUID.equals(senderUUID)){
					InetSocketAddress teamMemberAddress = gameServer.getConnectionManager().getAddressByUUID(teamMemberUUID);
					
					System.out.println("Sending message to "+gameServer.getGameManager().getPlayer(teamMemberUUID).nickname);
					
					gameServer.getConnectionManager().messageSenderList.get(teamMemberAddress).sendMessage(message);				
				}
			}
		}
	}
	
	public static void broadcastMessage(GameServer gameServer, String message, UUID senderUUID) throws IOException{
		
		
		ArrayList<MessageSender> values_Copy = new ArrayList<>(gameServer.getConnectionManager().messageSenderList.values());
				
		for(MessageSender sender : values_Copy){
			
			if(senderUUID != null){
				if(!sender.client_UUID.equals(senderUUID))
					sender.sendMessage(message);
			}
			else{
				sender.sendMessage(message);
			}
		}
	}
	
	private void sendMessage(String message){
		if(message!=null)
			try {
				toClient_Writer.write(message+'\n');
				toClient_Writer.flush();
				
			}		
			catch (IOException e) {
				System.out.println("sendMessage ERROR ! : "+e.toString());
				System.out.println("sendMessage ERROR ! : original message : "+message);
				System.out.println("MessageSender : sendMessage : removing player");
				
				getGameServer().getConnectionManager().closeConnection(client_Address);
			}
	}
	
	private void sendMessage(Map<String, Object> jsonStructure){
		try {
			sendMessage(getGameServer().getMessageSenderUtils().mapper.writeValueAsString(jsonStructure)+'\n');
		} 
		
		catch (JsonProcessingException e) {
			
			System.out.println("sendmMessage : Json Error : "+e.toString());
			
		}
	}
	
	private void print(String message){
		System.out.println(message);		
	}
	
	
	
	
	
	public static void send_CHANGE_POSITION(GameServer gameServer, UUID client_UUID, double latitude, double longitude){
		System.out.println("Sending CHANGE_POSITION");
				
		try {
			broadcastMessage(
					gameServer,					
					gameServer.getMessageSenderUtils().composeMessage(Messages.InGame.FromServer.CHANGE_POSITION, 
					gameServer.getMessageSenderUtils().constructChangePositionString(client_UUID, latitude, longitude)
				),
				client_UUID
			);
		}
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_POSITION : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_CHANGE_POSITION : Network Error : "+e.toString());
		}
	}
	
	public void send_MESSAGE_TEAM(UUID client_UUID, String message){
		print("Sending MESSAGE_TEAM");
		
		int playersTeam = getGameServer().getGameManager().getTeam(client_UUID);
		
		if((playersTeam!=getGameServer().getGameManager().HOME_TEAM) && (playersTeam!=getGameServer().getGameManager().AWAY_TEAM)){
			System.out.println("MESSAGE TEAM FAILED ! player belongs to NO TEAM !");			
			return;	
		}
		
		try{
			String messageString = getGameServer().getMessageSenderUtils().constructTeamMessageString(client_UUID, message);			
			String messageToSend = getGameServer().getMessageSenderUtils().composeMessage(Messages.InGame.FromServer.MESSAGE_TEAM, messageString);
			
			multicastMessage(gameServer, messageToSend, playersTeam, client_UUID);			
		}
		
		catch (JsonProcessingException e) {
			System.out.println("send_MESSAGE_TEAM : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_MESSAGE_TEAM : Network Error : "+e.toString());
		}
	}
	
	public static void send_GAME_START(GameServer gameServer){
		System.out.println("Sending GAME_START");
		// IF THE MINIMUM DISTANCE BETWEEN PLAYERS OF BOTH TEAMS IS AT LEAST X (EG. 1000M), THE GAME SHOULD START		
		if(gameServer.getGameManager().game_started == false){
		
			gameServer.getGameManager().game_started = true;
			
			try {
				broadcastMessage(
						gameServer, 
						gameServer.getMessageSenderUtils().composeEmptyMessage(Messages.InGame.FromServer.GAME_START), 
						null
				);
			}
			
			catch (IOException e) {
				System.out.println("send_GAME_START : Network Error : "+e.toString());
			}
		}
	}
	
	public void send_SHOOT(UUID targetUUID, String weaponName, int damage){
		print("Sending SHOOT");
		
		try {
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
					Messages.InGame.FromServer.SHOOT,
					getGameServer().getMessageSenderUtils().constructShootString(client_UUID, targetUUID, weaponName, damage)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_SHOOT : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_SHOOT : Network Error : "+e.toString());
		}
	}
	
	
	
	
	
	public void send_CONFIGURATION(){
		print("Sending CONFIGURATION");
		try {
			sendMessage(getGameServer().getMessageSenderUtils().constructConfigurationJsonString(client_Address));
		} 
		catch (IOException e) {
			System.out.println("send_CONFIGURATION : Network Error : "+e.toString());
		}
	}
	
	public static void send_PLAYER_JOINED(GameServer gameServer, UUID client_UUID){
		System.out.println("Sending PLAYER_JOINED");
		
		try{
			Player player = gameServer.getGameManager().getPlayer(client_UUID);		
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(Messages.Lobby.FromServer.PLAYER_JOINED, 
					gameServer.getMessageSenderUtils().constructPlayerJoinedString(
								client_UUID, 
								player.nickname, 
								player.professionTitle, 
								gameServer.getGameManager().getTeam(client_UUID)
						)
				),
				client_UUID
			);
		}
		catch(IOException e){
			System.out.println("send_PLAYER_JOINED : Network Error : "+e.toString());
			
		}
	}
	
	public static void send_PLAYER_LEFT(GameServer gameServer, UUID client_UUID){
		System.out.println("Sending PLAYER_LEFT");
		
		try{
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
						Messages.Lobby.FromServer.PLAYER_LEFT, 
						gameServer.getMessageSenderUtils().constructPlayerLeftString(client_UUID)
					),
					client_UUID
			);
		}
		catch(IOException e){
			System.out.println("send_PLAYER_LEFT : Network Error : "+e.toString());			
		}
	}	
	
	public static void send_CHANGE_NAME(GameServer gameServer, UUID client_UUID, String newName){
		System.out.println("Sending CHANGE_NAME");		
		
		try {
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
						Messages.Lobby.FromServer.CHANGE_NAME, 
						gameServer.getMessageSenderUtils().constructChangeNameString(client_UUID, newName)
					),
					client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_NAME : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_CHANGE_NAME : Network Error : "+e.toString());
		}
		
	}
	
	public static void send_CHANGE_TEAM(GameServer gameServer, UUID client_UUID, int newTeam){
		System.out.println("Sending CHANGE_TEAM");

		try {
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
						Messages.Lobby.FromServer.CHANGE_TEAM, 
						gameServer.getMessageSenderUtils().constructChangeTeamString(client_UUID, newTeam)
					),
					client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_TEAM : Json Error : "+e.toString());
		}
		
		catch (IOException e) {
			System.out.println("send_CHANGE_TEAM : Network Error : "+e.toString());
		}
	}
	
	public void send_CHANGE_PROFESSION(String newProfession){
		print("Sending CHANGE_PROFESSION");
		
		try {
			broadcastMessage(
				getGameServer(),
				getGameServer().getMessageSenderUtils().composeMessage(
						Messages.Lobby.FromServer.CHANGE_PROFESSION, 
						getGameServer().getMessageSenderUtils().constructChangeProfessionString(client_UUID, newProfession)
				),
				client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_CHANGE_PROFESSION : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_CHANGE_PROFESSION : Network Error : "+e.toString());
		}		
	}
	
	public static void send_MESSAGE_ALL(GameServer gameServer, UUID client_UUID, String message){
		System.out.println("Sending MESSAGE_ALL");
		
		try {
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
						Messages.Lobby.FromServer.MESSAGE_ALL, 
						gameServer.getMessageSenderUtils().constructMessageAll(client_UUID, message)
					),
					client_UUID
			);
		} 
		catch (JsonProcessingException e) {
			System.out.println("send_MESSAGE_ALL : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_MESSAGE_ALL : Network Error : "+e.toString());
		}
	}
	
	public static void send_ENTER_GAME(GameServer gameServer){
		System.out.println("Sending ENTER_GAME");
		
		gameServer.getGameManager().game_entered = true;
		
		try {
			broadcastMessage(gameServer, gameServer.getMessageSenderUtils().composeEmptyMessage(Messages.Lobby.FromServer.ENTER_GAME), null);
			//send_GAME_START(gameServer);
		} 
		
		catch (IOException e) {
			System.out.println("send_ENTER_GAME : Network Error : "+e.toString());
		}
	}
	
	public static void send_READY(GameServer gameServer, UUID client_UUID, boolean isPlayerReady){
		System.out.println("Sending READY");
		
		try {
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
							Messages.Lobby.FromServer.READY, 
							gameServer.getMessageSenderUtils().constructReadyString(client_UUID, isPlayerReady)
					),
					client_UUID
			);
		} 
		
		catch (JsonProcessingException e) {
			System.out.println("send_READY : Json Error : "+e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_READY : Network Error : "+e.toString());
		}
	}
	
	public static void send_COUNTDOWN(GameServer gameServer, int secondsLeft){
		System.out.println("Sending COUNTDOWN");
		
		try {			
			broadcastMessage(
					gameServer,
					gameServer.getMessageSenderUtils().composeMessage(
							Messages.Lobby.FromServer.COUNTDOWN, 
							gameServer.getMessageSenderUtils().constructCountdownString(secondsLeft)
					), 
					null
			);
		}
		
		catch (JsonProcessingException e) {
			System.out.println("send_COUNTDOWN : Json Error : " + e.toString());
		} 
		
		catch (IOException e) {
			System.out.println("send_COUNTDOWN : Network Error : " + e.toString());
		}
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
	
	
}
