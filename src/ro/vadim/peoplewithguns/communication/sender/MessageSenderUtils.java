package ro.vadim.peoplewithguns.communication.sender;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.messages.Messages;
import ro.vadim.peoplewithguns.communication.messages.Messages.Lobby;
import ro.vadim.peoplewithguns.communication.messages.Messages.Lobby.FromServer;
import ro.vadim.peoplewithguns.game.GameManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageSenderUtils {
	
	
	private GameServer gameServer = null;
	public  ObjectMapper mapper = null;
		
	
	public MessageSenderUtils(GameServer newGameServer){
		setGameServer(newGameServer);
		initObjectMapper();
	}
	
	
	
	public  void initObjectMapper(){
		mapper = new ObjectMapper();		
	}
	
	
	
	
	public  String composeEmptyMessage(int messageType){
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : {} }"+'\n';
	}
	
	public  String composeMessage(int messageType, Map<String, Object> innerStructure) throws JsonProcessingException{
		
		String data = mapper.writeValueAsString(innerStructure);
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : "+ data + " }"+'\n';
	}
	
	public  String composeMessage(int messageType, String data) throws JsonProcessingException{
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : "+ data + " }"+'\n';
	}
	
	
	
	
	
	
	//IN-GAME MESSAGE CONSTRUCTORS
	
	public  String constructChangePositionString(UUID playerUUID, double latitude, double longitude) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("latitude", latitude);
		jsonStructure.put("longitude", longitude);
		
		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	public  String constructShootString(UUID playerUUID, UUID targetUUID, String weaponName, int damage) throws JsonProcessingException{		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("target", targetUUID);
		jsonStructure.put("weapon", weaponName);
		jsonStructure.put("damage", damage);
		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	
	//LOBBY MESSAGE CONSTRUCTORS
	
	public  String constructProfessionsAndWeaponsJsonString() throws IOException{
		
		String myCurrentDir = System.getProperty("user.dir");
		System.out.println("Current Directory : "+myCurrentDir);
				
				
		File configJson = null;
		
		if(getGameServer().getGameType() == null){
			System.out.println("GAME TYPE = null. Sending default");
			configJson = new File(myCurrentDir+"/config.json");
		}
		
		
		else if(getGameServer().getGameType().intValue() == GameServer.GAME_TYPE_NORMAL){
			System.out.println("GAME TYPE = normal. Sending default");
			configJson = new File(myCurrentDir+"/config.json");
		}
		
		else if(getGameServer().getGameType().intValue() == GameServer.GAME_TYPE_DUEL){
			System.out.println("GAME TYPE = DUEL. Sending DUEL GAME");
			configJson = new File(myCurrentDir+"/config_duel.json");
		}
		
		else if(getGameServer().getGameType().intValue() == GameServer.GAME_TYPE_DAVID_VS_GOLIATH){
			System.out.println("GAME TYPE = DAVID vs. GOLIATH. Sending DAVID VS GOLIATH");
			configJson = new File(myCurrentDir+"/config_david_vs_goliath.json");
		}
				
				
		String configJsonString = "";
		BufferedReader reader = new BufferedReader(new FileReader(configJson));
		
		String aux = "";
		while((aux = reader.readLine()) != null){
			configJsonString += aux;
		}
		
		return configJsonString;
	}
	
	public  String constructPlayerListJsonString(UUID forWhichPlayer) throws JsonProcessingException{
		int playersTeam = getGameServer().getGameManager().getTeam(forWhichPlayer);
		if(playersTeam == -1) return null;
		
		String homeTeamPlayerListJson = mapper.writeValueAsString(getGameServer().getGameManager().home_team);
		String awayTeamPlayerListJson = mapper.writeValueAsString(getGameServer().getGameManager().away_team);
		
		String jsonString = "";
		
		if(playersTeam == getGameServer().getGameManager().HOME_TEAM)
			jsonString = "{ \"friends\" : "+homeTeamPlayerListJson+", "+" \"enemies\" : "+awayTeamPlayerListJson+"}";
		else 
			jsonString = "{ \"friends\" : "+awayTeamPlayerListJson+", "+" \"enemies\" : "+homeTeamPlayerListJson+"}";
		
		return jsonString;
	}
		
	public  String constructConfigurationJsonString(InetSocketAddress client_Address) throws IOException{
		UUID client_UUID = getGameServer().getConnectionManager().getUuidByAddress(client_Address);
		
		if(client_UUID == null)
			System.out.println("Client UUID : null");
		else
			System.out.println("Client UUID : "+ client_UUID.toString());
		
		int client_team = getGameServer().getGameManager().getTeam(client_UUID);
		
		String weaponsAndProfessionsString = constructProfessionsAndWeaponsJsonString();
				
		String playersJsonString = constructPlayerListJsonString(client_UUID);
		
				
		String configurationJsonString = "{   \"your_id\" : \"" + client_UUID.toString() + "\", "+
											" \"your_team\" : " + String.valueOf(client_team) + ", "+ 
											" \"professions_and_weapons\" : " + weaponsAndProfessionsString + ", "+
											" \"players\" : " + playersJsonString + " }";
		
		String configurationMessage = "{ \"messageType\" : "+ String.valueOf(Messages.Lobby.FromServer.CONFIGURATION) + ", \"data\" : " + configurationJsonString + " }";
		
		return configurationMessage;
	}

	public  String constructPlayerJoinedString(UUID playerUUID, String playerNickname, String playerProfession, int playerTeam) throws JsonProcessingException{		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("nickname", playerNickname);
		jsonStructure.put("uuid", playerUUID);
		jsonStructure.put("profession", playerProfession);
		jsonStructure.put("team", String.valueOf(playerTeam));
		
		return mapper.writeValueAsString(jsonStructure);
	}

	public  String constructPlayerLeftString(UUID playerUUID) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		
		return mapper.writeValueAsString(jsonStructure);
		
	}
	
	public  String constructChangeNameString(UUID playerUUID, String newNickname) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("nickname", newNickname);
				
		return mapper.writeValueAsString(jsonStructure);
	}
	
	public  String constructChangeTeamString(UUID playerUUID, int newTeam) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
				
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("team", String.valueOf(newTeam));
		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	public  String constructChangeProfessionString(UUID playerUUID, String newProfession) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("profession", newProfession);
		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	public  String constructReadyString(UUID playerUUID, boolean isPlayerReady) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("ready", String.valueOf(isPlayerReady));
		
		return mapper.writeValueAsString(jsonStructure);		
	}
	
	public  String constructMessageAll(UUID playerUUID, String message) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("message", message);
		
		return mapper.writeValueAsString(jsonStructure);
	}
	
	public  String constructCountdownString(int secondsLeft) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("secondsLeft", String.valueOf(secondsLeft));		
		
		return mapper.writeValueAsString(jsonStructure);	
	}
	
	public  String constructTeamMessageString(UUID playerUUID, String message) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("message", message);
		
		return mapper.writeValueAsString(jsonStructure);
		
	}



	public GameServer getGameServer() {
		return gameServer;
	}



	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
	
}
