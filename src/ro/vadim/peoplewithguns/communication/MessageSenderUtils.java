package ro.vadim.peoplewithguns.communication;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import ro.vadim.peoplewithguns.game.gameelements.GameManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MessageSenderUtils {
	
	public static ObjectMapper mapper = null;
		
	
	
	public void initObjectMapper(){
		MessageSenderUtils.mapper = new ObjectMapper();		
	}
	
	
	
	
	public static String composeEmptyMessage(int messageType){
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : {} }"+'\n';
	}
	
	public static String composeMessage(int messageType, Map<String, Object> innerStructure) throws JsonProcessingException{
		
		String data = mapper.writeValueAsString(innerStructure);
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : "+ data + " }"+'\n';
	}
	
	public static String composeMessage(int messageType, String data) throws JsonProcessingException{
		
		return "{ \"messageType\" : " + String.valueOf(messageType)+", "+ " \"data\" : "+ data + " }"+'\n';
	}
	
	
	
	
	
	
	//IN-GAME MESSAGE CONSTRUCTORS
	
	public static String constructChangePositionString(UUID playerUUID, double latitude, double longitude) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("latitude", latitude);
		jsonStructure.put("longitude", longitude);
		
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
	}
	
	public static String constructShootString(UUID playerUUID, UUID targetUUID, String weaponName, int damage) throws JsonProcessingException{		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("target", targetUUID);
		jsonStructure.put("weapon", weaponName);
		jsonStructure.put("damage", damage);
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
	}
	
	
	//LOBBY MESSAGE CONSTRUCTORS
	
	public static String constructProfessionsAndWeaponsJsonString() throws IOException{
		
		File configJson = new File("C:\\Users\\Vadim\\workspace_Android\\GameServer\\config.json");
				
		String configJsonString = "";
		BufferedReader reader = new BufferedReader(new FileReader(configJson));
		
		String aux = "";
		while((aux = reader.readLine()) != null){
			configJsonString += aux;
		}
		
		return configJsonString;
	}
	
	public static String constructPlayerListJsonString(UUID forWhichPlayer) throws JsonProcessingException{
		int playersTeam = GameManager.getTeam(forWhichPlayer);
		if(playersTeam == -1) return null;
		
		String homeTeamPlayerListJson = mapper.writeValueAsString(GameManager.home_team);
		String awayTeamPlayerListJson = mapper.writeValueAsString(GameManager.away_team);
		
		String jsonString = "";
		
		if(playersTeam == GameManager.HOME_TEAM)
			jsonString = "{ \"friends\" : "+homeTeamPlayerListJson+", "+" \"enemies\" : "+awayTeamPlayerListJson+"}";
		else 
			jsonString = "{ \"friends\" : "+awayTeamPlayerListJson+", "+" \"enemies\" : "+homeTeamPlayerListJson+"}";
		
		return jsonString;
	}
		
	public static String constructConfigurationJsonString(InetAddress client_Address) throws IOException{
		UUID client_UUID = ConnectionManager.getUuidByAddress(client_Address);
		
		if(client_UUID == null)
			System.out.println("Client UUID : null");
		else
			System.out.println("Client UUID : "+ client_UUID.toString());
		
		int client_team = GameManager.getTeam(client_UUID);
		
		String weaponsAndProfessionsString = constructProfessionsAndWeaponsJsonString();
				
		String playersJsonString = constructPlayerListJsonString(client_UUID);
		
		String configurationJsonString = "{   \"your_id\" : \"" + client_UUID.toString() + "\", "+
											" \"your_team\" : " + String.valueOf(client_team) + ", "+ 
											" \"professions_and_weapons\" : " + weaponsAndProfessionsString + ", "+
											" \"players\" : " + playersJsonString + " }";
		
		String configurationMessage = "{ \"messageType\" : "+ String.valueOf(Messages.Lobby.FromServer.CONFIGURATION) + ", \"data\" : " + configurationJsonString + " }";
		
		return configurationMessage;
	}

	public static String constructPlayerJoinedString(UUID playerUUID, String playerNickname, String playerProfession, int playerTeam) throws JsonProcessingException{		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("nickname", playerNickname);
		jsonStructure.put("uuid", playerUUID);
		jsonStructure.put("profession", playerProfession);
		jsonStructure.put("team", String.valueOf(playerTeam));
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
		
	}

	public static String constructPlayerLeftString(UUID playerUUID) throws JsonProcessingException{
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
		
	}
	
	public static String constructChangeNameString(UUID playerUUID, String newNickname) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("nickname", newNickname);
				
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
	}
	
	public static String constructChangeTeamString(UUID playerUUID, int newTeam) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
				
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("team", String.valueOf(newTeam));
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
	}
	
	public static String constructChangeProfessionString(UUID playerUUID, String newProfession) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("profession", newProfession);
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
	}
	
	public static String constructReadyString(UUID playerUUID, boolean isPlayerReady) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("ready", String.valueOf(isPlayerReady));
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);		
	}
	
	public static String constructMessageAll(UUID playerUUID, String message) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("player", playerUUID);
		jsonStructure.put("message", message);
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);
	}
	
	public static String constructCountdownString(int secondsLeft) throws JsonProcessingException{
		
		LinkedHashMap<String, Object> jsonStructure = new LinkedHashMap<String, Object>();
		
		jsonStructure.put("secondsLeft", String.valueOf(secondsLeft));		
		
		return MessageSenderUtils.mapper.writeValueAsString(jsonStructure);	
	}
	
}
