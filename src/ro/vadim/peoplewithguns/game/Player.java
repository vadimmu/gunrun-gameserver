package ro.vadim.peoplewithguns.game;

import java.util.ArrayList;



public class Player {
	
	public static final int STATUS_SELECTED = -1;	
	public static final int STATUS_CURRENT_PLAYER = 0;
	public static final int STATUS_FRIEND = 1;
	public static final int STATUS_ENEMY = 2;
	
	public String nickname = null;
	public String professionTitle = null;
	public Integer health = null;		
	
	public Double latitude = null;
	public Double longitude = null;
	
	
	public Player(String newPlayerNickname, int newHealth, String newProfessionTitle, Double newLatitude, Double newLongitude){				
		
		this.professionTitle = newProfessionTitle;
		this.nickname = newPlayerNickname;
		this.latitude = newLatitude;
		this.longitude = newLongitude;
		this.health = newHealth;
	}
	
}
