package ro.vadim.peoplewithguns.game.gameelements;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.communication.CountdownToGame;
import ro.vadim.peoplewithguns.communication.MessageSender;

public class GameManager {
	
	public static final int HOME_TEAM = 0;
	public static final int AWAY_TEAM = 1;	
	
	public static HashMap<UUID, Player> home_team = new HashMap<UUID, Player>(ConnectionManager.EXPECTED_CONNECTIONS/2);
	public static HashMap<UUID, Player> away_team = new HashMap<UUID, Player>(ConnectionManager.EXPECTED_CONNECTIONS/2);
	
	public static HashMap<UUID, Boolean> playersReady = new HashMap<UUID, Boolean>(ConnectionManager.EXPECTED_CONNECTIONS);
	
	private static Thread countdownThread = null;
	private static CountdownToGame countdown = null;
	
	
	private static void attemptToStopPreviousCountdown() throws InterruptedException{
		
		if(countdown != null){			
			countdown.stop();
			countdownThread.interrupt();
		}
	}
	
	private static void startNewCountdown(){
		countdown = new CountdownToGame();				
		countdownThread = new Thread(countdown);
		countdownThread.start();
		System.out.println("countdown thread started !");
	}
	
	public static void startCountdown(){
					
		try {						
			attemptToStopPreviousCountdown();			
		}					
		
		catch (InterruptedException e) {
			System.out.println("startCountdown(stopping previous countdown) : could not stop previous countdown !");
			System.out.println("startCountdown(stopping previous countdown) : " + e.toString());
		}
		
		startNewCountdown();
		
	}
	
	
	
	
		
	public static synchronized void setPlayerReady(UUID playerUUID, boolean ready){
		if(getPlayer(playerUUID) != null){
			playersReady.put(playerUUID, ready);			
		}
	}
		
	private static synchronized int countReadyPlayers(){
		int count = 0;
		for(Boolean b : playersReady.values()){
			if(b.equals(Boolean.TRUE))
				count++;		
		}		
		return count;
	}
	
	public static synchronized boolean areAllPlayersReady(){
		if(countReadyPlayers() == (home_team.size() + away_team.size()))
			return true;
		
		return false;
	}
	
	
	
	
	
	/**returns 0 (home team), 1(away team) or -1(could not be found)**/
	public static int getTeam(UUID playerUUID){
		if(home_team.containsKey(playerUUID))
			return HOME_TEAM;
		if(away_team.containsKey(playerUUID))
			return AWAY_TEAM;
		
		return -1;
	}
		
	public static Player getPlayer(UUID playerUUID){
		
		if(home_team.containsKey(playerUUID))
			return home_team.get(playerUUID);
		
		else if(away_team.containsKey(playerUUID))
			return away_team.get(playerUUID);
		
		else return null;
	}
	
	public static Player getPlayerByAddress(InetAddress playerAddress){
		
		if(!ConnectionManager.connections.containsKey(playerAddress))
			return null;
		else
			return getPlayer(ConnectionManager.connections.get(playerAddress));		
	}
	
	
	
	
	
	public static synchronized void addPlayer(Player newPlayer, UUID newPlayerUUID){
		
		if((newPlayer == null) || (newPlayerUUID == null))
			return;
		
		int team = 0;
		
		if(home_team.size() <= away_team.size()){
			home_team.put(newPlayerUUID, newPlayer);
			team = GameManager.HOME_TEAM;
			System.out.println("Adding player to Home Team");
		}
		
		else{ 
			away_team.put(newPlayerUUID, newPlayer);
			team = GameManager.AWAY_TEAM;
			System.out.println("Adding player to Away Team");
		}
	}
	
	public static synchronized void movePlayer(UUID playerUUID){
		
		Player player = null;
		
		if(home_team.containsKey(playerUUID)){
			
			player = home_team.get(playerUUID);
			away_team.put(playerUUID, player);
			home_team.remove(playerUUID);
			
			
		}
		else if(away_team.containsKey(playerUUID)){
			player = away_team.get(playerUUID);
			home_team.put(playerUUID, player);
			away_team.remove(playerUUID);
		}
		else{
			System.out.println("movePlayer : player does not exist! could not move the player.");			
		}
	}
		
	public static synchronized void deletePlayer(UUID playerUUID){
		System.out.println("deletePlayer : EXISTING PLAYERS : ");
		System.out.println("----------------------------------");
		for(Entry e : home_team.entrySet()){			
			System.out.println(e.getKey().toString()+" - "+((Player) e.getValue()).nickname);
		}
		
		if(home_team.containsKey(playerUUID))
			home_team.remove(playerUUID);
		
		else if(away_team.containsKey(playerUUID))
			away_team.remove(playerUUID);
		
		else 
			System.out.println("deletePlayer : player does not exist! could not delete the player.");
		
	}
	
	public static synchronized void changePlayerName(UUID playerUUID, String newNickname){
		getPlayer(playerUUID).nickname = newNickname;		
	}
	
	public static synchronized void changePlayerProfession(UUID playerUUID, String newProfession){
		getPlayer(playerUUID).professionTitle = newProfession;
	}
	
	public static synchronized void changePlayerTeam(UUID playerUUID){
		
		
		GameManager.movePlayer(playerUUID);
		
	}
	
	
		
	
	
	
}
