package ro.vadim.peoplewithguns.game.gameelements;

import java.util.ArrayList;



public class Player {
	
	public static final int STATUS_SELECTED = -1;	
	public static final int STATUS_CURRENT_PLAYER = 0;
	public static final int STATUS_FRIEND = 1;
	public static final int STATUS_ENEMY = 2;
	
	public String nickname = null;		
	public String professionTitle = null;	
	public Integer health = null;		
	
	public double latitude = 0;
	public double longitude = 0;
	
	
	public Player(String newPlayerNickname, int newHealth, String newProfessionTitle, double newLatitude, double newLongitude){				
		
		this.professionTitle = newProfessionTitle;
		this.nickname = newPlayerNickname;
		this.latitude = newLatitude;
		this.longitude = newLongitude;
		this.health = newHealth;
	}
	
}
