package ro.vadim.peoplewithguns.game;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.UUID;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.communication.GameServer;
import ro.vadim.peoplewithguns.communication.runnables.CountdownToGame;
import ro.vadim.peoplewithguns.communication.sender.MessageSender;

public class GameManager {
	
	private GameServer gameServer = null;
	
	
	public static final int MINIMUM_INTER_TEAM_DISTANCE = 150;
	public static final int MAXIMUM_INTRA_TEAM_DISTANCE = 50;
	
	public static final int HOME_TEAM = 0;
	public static final int AWAY_TEAM = 1;
	
	public HashMap<UUID, Player> home_team = null; 
	public HashMap<UUID, Player> away_team = null; 
	
	public HashMap<UUID, Boolean> playersReady = null;
	
	private Thread countdownThread = null;
	private CountdownToGame countdown = null;
	
	public boolean game_entered = false;
	public boolean game_started = false;
		
	
	
	
	public GameManager(GameServer newGameServer){
		
		setGameServer(newGameServer);
		
		home_team = new HashMap<UUID, Player>(getGameServer().getConnectionManager().EXPECTED_CONNECTIONS/2);
		away_team = new HashMap<UUID, Player>(getGameServer().getConnectionManager().EXPECTED_CONNECTIONS/2);		
		playersReady = new HashMap<UUID, Boolean>(getGameServer().getConnectionManager().EXPECTED_CONNECTIONS);		
		
	}
	
	
	
	
	
	
	private void attemptToStopPreviousCountdown() throws InterruptedException{
		
		if(countdown != null){			
			countdown.stop();
			countdownThread.interrupt();
		}
	}
	
	private void startNewCountdown(){
		countdown = new CountdownToGame(getGameServer());
		countdownThread = new Thread(countdown);
		countdownThread.start();
		System.out.println("countdown thread started !");
	}
	
	public void startCountdown(){
					
		try {						
			attemptToStopPreviousCountdown();
		}					
		
		catch (InterruptedException e) {
			System.out.println("startCountdown(stopping previous countdown) : could not stop previous countdown !");
			System.out.println("startCountdown(stopping previous countdown) : " + e.toString());
		}
		
		startNewCountdown();
		
	}
	
	public void stopCountdown(){
		
		try {						
			attemptToStopPreviousCountdown();			
		}					
		
		catch (InterruptedException e) {
			System.out.println("stopCountdown(stopping previous countdown) : could not stop previous countdown !");
			System.out.println("stopCountdown(stopping previous countdown) : " + e.toString());
		}
	}
	
	
	
		
	public synchronized void setPlayerReady(UUID playerUUID, boolean ready){
		if(getPlayer(playerUUID) != null){
			playersReady.put(playerUUID, ready);			
		}
	}
		
	private synchronized int countReadyPlayers(){
		int count = 0;
				
		for(Boolean b : playersReady.values()){
			if(b.booleanValue() == true)
				count++;
		}		
		return count;
	}
	
	
	public synchronized boolean isEverybodyReady(){
		boolean playersAreReady = true;
		
		System.out.println("-----------------------");
		System.out.println("Are all players ready ?");
		
		if((home_team.size() == 0)&&(away_team.size() == 0)){			
			System.out.println("There are no players left on the server! ");
			return false;		
		}
		
		
		for(Entry e : home_team.entrySet()){
			System.out.println(((Player)e.getValue()).nickname+" : "+playersReady.get(e.getKey()));
			
			playersAreReady &= playersReady.get(e.getKey()); 
			
		}
		
		for(Entry e : away_team.entrySet()){
			System.out.println(((Player)e.getValue()).nickname+" : "+playersReady.get(e.getKey()));
			
			playersAreReady &= playersReady.get(e.getKey());
		}
		
		System.out.println("-----------------------");
		
		return playersAreReady;
	}
	
	
	public synchronized boolean areAllPlayersReady(){
		
		
		
		System.out.println("-----------------------");
		System.out.println("Are all players ready ?");
		
		for(Entry e : home_team.entrySet()){
			System.out.println(((Player)e.getValue()).nickname+" : "+playersReady.get(e.getKey()));
			
		}
		
		for(Entry e : away_team.entrySet()){
			System.out.println(((Player)e.getValue()).nickname+" : "+playersReady.get(e.getKey()));
		}
		
		System.out.println("-----------------------");		
		
		if(countReadyPlayers() == (home_team.size() + away_team.size()))
			return true;
		
		return false;
	}
	
	public synchronized void setAllPlayersReady(boolean ready){
		
		for(UUID playerUUID : home_team.keySet()){
			setPlayerReady(playerUUID, ready);
		}
		
		for(UUID playerUUID : away_team.keySet()){
			setPlayerReady(playerUUID, ready);
		}
	}
	
	
	
	
	
	/**returns 0 (home team), 1(away team) or -1(could not be found)**/
	public int getTeam(UUID playerUUID){
		if(home_team.containsKey(playerUUID))
			return HOME_TEAM;
		if(away_team.containsKey(playerUUID))
			return AWAY_TEAM;
		
		return -1;
	}
		
	public Player getPlayer(UUID playerUUID){
		
		if(home_team.containsKey(playerUUID))
			return home_team.get(playerUUID);
		
		else if(away_team.containsKey(playerUUID))
			return away_team.get(playerUUID);
		
		else return null;
	}
	
	public Player getPlayerByAddress(InetSocketAddress playerAddress){
		
		if(!getGameServer().getConnectionManager().connections.containsKey(playerAddress))
			return null;
		else
			return getPlayer(getGameServer().getConnectionManager().connections.get(playerAddress));		
	}
	
	
	
	
	
	public synchronized void addPlayer(Player newPlayer, UUID newPlayerUUID){
		
		if((newPlayer == null) || (newPlayerUUID == null))
			return;
		
		int team = 0;
		
		if(home_team.size() <= away_team.size()){
			home_team.put(newPlayerUUID, newPlayer);
			team = GameManager.HOME_TEAM;
			System.out.println("Adding player to Home Team");
		}
		
		else{ 
			away_team.put(newPlayerUUID, newPlayer);
			team = GameManager.AWAY_TEAM;
			System.out.println("Adding player to Away Team");
		}
		
		playersReady.put(newPlayerUUID, false);
	}
	
	public synchronized void movePlayer(UUID playerUUID){
		
		Player player = null;
		
		if(home_team.containsKey(playerUUID)){
			
			player = home_team.get(playerUUID);
			away_team.put(playerUUID, player);
			home_team.remove(playerUUID);
			
			
		}
		else if(away_team.containsKey(playerUUID)){
			player = away_team.get(playerUUID);
			home_team.put(playerUUID, player);
			away_team.remove(playerUUID);
		}
		else{
			System.out.println("movePlayer : player does not exist! could not move the player.");			
		}
	}
		
	public synchronized void deletePlayer(UUID playerUUID){
		System.out.println("deletePlayer : EXISTING PLAYERS : ");
		System.out.println("----------------------------------");
		for(Entry e : home_team.entrySet()){			
			System.out.println(e.getKey().toString()+" - "+((Player) e.getValue()).nickname);
		}
		
		if(playersReady.containsKey(playerUUID))
			playersReady.remove(playerUUID);
		
		if(home_team.containsKey(playerUUID))
			home_team.remove(playerUUID);
		
		else if(away_team.containsKey(playerUUID))
			away_team.remove(playerUUID);
		
		else{ 
			System.out.println("deletePlayer : player does not exist! could not delete the player.");
			
			InetSocketAddress clientAddress = getGameServer().getConnectionManager().getAddressByUUID(playerUUID);
			if(clientAddress != null){
				
				System.out.println("deletePlayer : cleaning up after \"non-existing\" player... ");
				getGameServer().getConnectionManager().closeConnection(clientAddress);
			}
		}
		
	}
	
	public synchronized void setPlayerName(UUID playerUUID, String newNickname){
		getPlayer(playerUUID).nickname = newNickname;		
	}
	
	public synchronized void setPlayerProfession(UUID playerUUID, String newProfession){
		getPlayer(playerUUID).professionTitle = newProfession;
	}
	
	public synchronized void changePlayerTeam(UUID playerUUID){				
		getGameServer().getGameManager().movePlayer(playerUUID);
	}
	
	public synchronized void setPlayerPosition(UUID playerUUID, double newLatitude, double newLongitude){
		Player player = getPlayer(playerUUID);
		player.latitude = newLatitude;
		player.longitude = newLongitude;		
	}
	
	
	public synchronized double getDistanceBetweenPoints(double point1_latitude, double point1_longitude, double point2_latitude, double point2_longitude){		
		return Math.sqrt(Math.pow((point1_latitude - point2_latitude), 2) + Math.pow((point1_longitude - point2_longitude), 2));
	}
	
	public synchronized boolean isGameStartConditionMet(){
		if((away_team.size() + home_team.size()) == 1){
			return true;
		}
		
		else{
			
			double home_team_average_latitude = 0;
			double home_team_average_longitude = 0;
			
			double away_team_average_latitude = 0;
			double away_team_average_longitude = 0;
			
			
			
			for(Player p1 : home_team.values()){
				if((p1.latitude == null)||(p1.longitude == null))
					return false;
			}
			
			for(Player p2 : away_team.values()){
				if((p2.latitude == null)||(p2.longitude == null))
					return false;
			}
			
			for(Player p1 : home_team.values()){
				home_team_average_latitude += p1.latitude;
				home_team_average_longitude += p1.longitude;
			}
			home_team_average_latitude = home_team_average_latitude / home_team.size();
			home_team_average_longitude = home_team_average_longitude / home_team.size();
			
			
			for(Player p2 : away_team.values()){
				away_team_average_latitude += p2.latitude;
				away_team_average_longitude += p2.longitude;
			}
			
			away_team_average_latitude = away_team_average_latitude / away_team.size();
			away_team_average_longitude = away_team_average_longitude / away_team.size();
			
			for(Player p1 : home_team.values()){
				if(getDistanceBetweenPoints(p1.latitude, p1.longitude, home_team_average_latitude, home_team_average_longitude) > MAXIMUM_INTRA_TEAM_DISTANCE / 2)
					return false;
			}
			
			for(Player p2 : away_team.values()){
				if(getDistanceBetweenPoints(p2.latitude, p2.longitude, away_team_average_latitude, away_team_average_longitude) > MAXIMUM_INTRA_TEAM_DISTANCE / 2)
					return false;
			}
			
			if(getDistanceBetweenPoints(home_team_average_latitude, home_team_average_longitude, away_team_average_latitude, away_team_average_longitude) <= MINIMUM_INTER_TEAM_DISTANCE){
				
				return true;
			}
			else 
				return false;			
		}
	}

	public GameServer getGameServer() {
		return gameServer;
	}

	public void setGameServer(GameServer gameServer) {
		this.gameServer = gameServer;
	}
	
	
		
	
	
	
}
