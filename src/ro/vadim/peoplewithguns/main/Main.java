package ro.vadim.peoplewithguns.main;

import java.io.IOException;
import java.net.SocketException;

import ro.vadim.peoplewithguns.communication.ConnectionManager;
import ro.vadim.peoplewithguns.gamesmanagement.TcpServer;

public class Main {
	
	public static TcpServer tcpServer = null;
	public static Thread serverMainThread = null;
	
	public static void main(String[] args){
		
		System.out.println("Hello !");
		Integer port = null;		
		
		try{
			port = Integer.valueOf(args[0]);
		}
		catch(Exception e){
			
			System.out.println("Could not get argument ! Reason : "+e.toString());
			port = null;
		}
		
			
		tcpServer = new TcpServer();
		
		if(port != null)
			tcpServer.PORT = port;
		
		serverMainThread = new Thread(tcpServer);
		serverMainThread.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			
			@Override
			public void run() {
								
				System.out.println("SHUTTING DOWN SERVER !");
				System.out.println("CLOSING ALL CONNECTIONS");
				
			}
		}));
		
	}	
}
